/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <ros/ros.h>
#include <autoware_msgs/LaneArray.h>
#include <optimal_vel_lib.h>


autoware_msgs::Lane compute_time_optimal_vel(autoware_msgs::Lane wp_in)
{
  int length = wp_in.waypoints.size();
  float* x = new float[length];
  float* y = new float[length];
  float* z = new float[length];

  float* limit_curve = new float[length];
  float* velocity = new float[length];
  float* time = new float[length];
  float* acc = new float[length];

  for(int i = 0; i < length; i++)
  {
    x[i] = wp_in.waypoints.at(i).pose.pose.position.x;
    y[i] = wp_in.waypoints.at(i).pose.pose.position.y;
    z[i] = wp_in.waypoints.at(i).pose.pose.position.z;
  }
  float init_velocity = 0;
  float final_velocity = 0;
  float f_max = 5000,  mass = 1000,  vel_max = 25,  friction_coefficient = 1.0,  height = 1.0,  width = 2.0;

  compute_limit_curve_and_velocity(length, x, y, z, init_velocity, final_velocity,
    f_max, mass, vel_max, friction_coefficient, height, width,
    limit_curve, velocity, time,acc);

  autoware_msgs::Lane wp_out = wp_in;
  //printf("velocity: ");
  for(int i = 0; i < length; i++)
  {
    wp_out.waypoints.at(i).twist.twist.linear.x = velocity[i];
    //printf(", %f",velocity[i]);
  }
  //printf("\n");
  //ROS_WARN_STREAM("wp_in*****************/n"<<wp_in<<"/n wp_out*****************/n"<<wp_out);
  return wp_out;
}


class ComputeVel
{
public:
  ComputeVel();
  ~ComputeVel();
private:
  ros::NodeHandle n;
  ros::Publisher pub_waypoints_,pub_waypoints_array_;
  ros::Subscriber lane_sub_;//,sub_pose_,sub_vel_;

  autoware_msgs::Lane lane,wp_out_;

  void laneCallback(const autoware_msgs::LaneArray::ConstPtr& lane_array);
bool compute_optimal_vel_flag_ ;
};

ComputeVel::ComputeVel()
{
  compute_optimal_vel_flag_ = true;
  pub_waypoints_ = n.advertise<autoware_msgs::Lane>("trajectory_waypoints", 1);
  //pub_waypoints_array_ = n.advertise<autoware_msgs::LaneArray>("lane_waypoints_array", 1);
  lane_sub_ = n.subscribe("/based/lane_waypoints_raw", 1, &ComputeVel::laneCallback, this);
}
ComputeVel::~ComputeVel()
{
}




void ComputeVel::laneCallback(const autoware_msgs::LaneArray::ConstPtr& lane_array)
{
  lane = lane_array->lanes[0];
  if(compute_optimal_vel_flag_)
    wp_out_ = compute_time_optimal_vel(lane); 
  else
  {
    wp_out_ = lane;
  }
  //pub_waypoints_array_.publish(*lane_array);
  pub_waypoints_.publish(wp_out_);
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "compute_vel");
  ComputeVel CV;
  ros::spin();

  return 0;
}
