/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <ros/ros.h>
#include <autoware_msgs/LaneArray.h>
#include "libwaypoint_follower/libwaypoint_follower.h"
#include <optimal_vel_lib.h>




// void displayDetectionRange(const autoware_msgs::Lane& lane, const int closest_waypoint,
//                            const ros::Publisher& detection_range_pub)
// {
//   // set up for marker array
//   visualization_msgs::MarkerArray marker_array;
//   visualization_msgs::Marker crosswalk_marker;
//   visualization_msgs::Marker waypoint_marker_stop;
//   visualization_msgs::Marker waypoint_marker_decelerate;
//   visualization_msgs::Marker stop_line;
//   crosswalk_marker.header.frame_id = "/map";
//   crosswalk_marker.header.stamp = ros::Time();
//   crosswalk_marker.id = 0;
//   crosswalk_marker.type = visualization_msgs::Marker::SPHERE_LIST;
//   crosswalk_marker.action = visualization_msgs::Marker::ADD;
//   waypoint_marker_stop = crosswalk_marker;
//   waypoint_marker_decelerate = crosswalk_marker;
//   stop_line = crosswalk_marker;
//   stop_line.type = visualization_msgs::Marker::CUBE;

//   // set each namespace
//   crosswalk_marker.ns = "Crosswalk Detection";
//   waypoint_marker_stop.ns = "Stop Detection";
//   waypoint_marker_decelerate.ns = "Decelerate Detection";
//   stop_line.ns = "Stop Line";

//   // set scale and color
//   double scale = 2 * stop_range;
//   waypoint_marker_stop.scale.x = scale;
//   waypoint_marker_stop.scale.y = scale;
//   waypoint_marker_stop.scale.z = scale;
//   waypoint_marker_stop.color.a = 0.2;
//   waypoint_marker_stop.color.r = 0.0;
//   waypoint_marker_stop.color.g = 1.0;
//   waypoint_marker_stop.color.b = 0.0;
//   waypoint_marker_stop.frame_locked = true;

//   scale = 2 * (stop_range + deceleration_range);
//   waypoint_marker_decelerate.scale.x = scale;
//   waypoint_marker_decelerate.scale.y = scale;
//   waypoint_marker_decelerate.scale.z = scale;
//   waypoint_marker_decelerate.color.a = 0.15;
//   waypoint_marker_decelerate.color.r = 1.0;
//   waypoint_marker_decelerate.color.g = 1.0;
//   waypoint_marker_decelerate.color.b = 0.0;
//   waypoint_marker_decelerate.frame_locked = true;

//   if (obstacle_waypoint > -1)
//   {
//     stop_line.pose.position = lane.waypoints[obstacle_waypoint].pose.pose.position;
//     stop_line.pose.orientation = lane.waypoints[obstacle_waypoint].pose.pose.orientation;
//   }
//   stop_line.pose.position.z += 1.0;
//   stop_line.scale.x = 0.1;
//   stop_line.scale.y = 15.0;
//   stop_line.scale.z = 2.0;
//   stop_line.lifetime = ros::Duration(0.1);
//   stop_line.frame_locked = true;
//   obstacleColorByKind(kind, stop_line.color, 0.3);

//   int crosswalk_id = crosswalk.getDetectionCrossWalkID();
//   if (crosswalk_id > 0)
//     scale = crosswalk.getDetectionPoints(crosswalk_id).width;
//   crosswalk_marker.scale.x = scale;
//   crosswalk_marker.scale.y = scale;
//   crosswalk_marker.scale.z = scale;
//   crosswalk_marker.color.a = 0.5;
//   crosswalk_marker.color.r = 0.0;
//   crosswalk_marker.color.g = 1.0;
//   crosswalk_marker.color.b = 0.0;
//   crosswalk_marker.frame_locked = true;

//   // set marker points coordinate
//   for (int i = 0; i < STOP_SEARCH_DISTANCE; i++)
//   {
//     if (closest_waypoint < 0 || i + closest_waypoint > static_cast<int>(lane.waypoints.size()) - 1)
//       break;

//     geometry_msgs::Point point;
//     point = lane.waypoints[closest_waypoint + i].pose.pose.position;

//     waypoint_marker_stop.points.push_back(point);

//     if (i > DECELERATION_SEARCH_DISTANCE)
//       continue;
//     waypoint_marker_decelerate.points.push_back(point);
//   }

//   if (crosswalk_id > 0)
//   {
//     if (!crosswalk.isMultipleDetection())
//     {
//       for (const auto& p : crosswalk.getDetectionPoints(crosswalk_id).points)
//         crosswalk_marker.points.push_back(p);
//     }
//     else
//     {
//       for (const auto& c_id : crosswalk.getDetectionCrossWalkIDs())
//       {
//         for (const auto& p : crosswalk.getDetectionPoints(c_id).points)
//         {
//           scale = crosswalk.getDetectionPoints(c_id).width;
//           crosswalk_marker.points.push_back(p);
//         }
//       }
//     }
//   }
//   // publish marker
//   marker_array.markers.push_back(crosswalk_marker);
//   marker_array.markers.push_back(waypoint_marker_stop);
//   marker_array.markers.push_back(waypoint_marker_decelerate);
//   if (kind != EControl::KEEP)
//     marker_array.markers.push_back(stop_line);
//   detection_range_pub.publish(marker_array);
//   marker_array.markers.clear();
// }


autoware_msgs::Lane compute_time_optimal_vel(autoware_msgs::Lane wp_in,geometry_msgs::TwistStamped current_velocity)
{
  int length = wp_in.waypoints.size();
  float* x = new float[length];
  float* y = new float[length];
  float* z = new float[length];

  float* limit_curve = new float[length];
  float* velocity = new float[length];
  float* time = new float[length];
  float* acc = new float[length];

  for(int i = 0; i < length; i++)
  {
    x[i] = wp_in.waypoints.at(i).pose.pose.position.x;
    y[i] = wp_in.waypoints.at(i).pose.pose.position.y;
    z[i] = wp_in.waypoints.at(i).pose.pose.position.z;
  }
  float init_velocity = current_velocity.twist.linear.x;
  float final_velocity = 0;
  float f_max = 1000,  mass = 1000,  vel_max = 15,  friction_coefficient = 1.0,  height = 1.0,  width = 2.0;

  compute_limit_curve_and_velocity(length, x, y, z, init_velocity, final_velocity,
    f_max, mass, vel_max, friction_coefficient, height, width,
    limit_curve, velocity, time,acc);

  autoware_msgs::Lane wp_out = wp_in;
  //printf("velocity: ");
  for(int i = 0; i < length; i++)
  {
    wp_out.waypoints.at(i).twist.twist.linear.x = velocity[i];
    //printf(", %f",velocity[i]);
  }
  //printf("\n");
  //ROS_WARN_STREAM("wp_in*****************/n"<<wp_in<<"/n wp_out*****************/n"<<wp_out);
  return wp_out;
}
class CompOptimalVelNode
{
public:
  CompOptimalVelNode();
  ~CompOptimalVelNode();
private:
  ros::NodeHandle n;
  ros::Publisher pub_waypoints_;
  ros::Subscriber waypoints_sub_,sub_pose_,sub_vel_;
  autoware_msgs::Lane wp_in_, wp_out_;
  geometry_msgs::PoseStamped current_pose_;
  geometry_msgs::TwistStamped current_velocity_;
 // void publishLaneArray();
  void callbackFromTwistStamped(const geometry_msgs::TwistStampedConstPtr &msg);
  void callbackFromPoseStamped(const geometry_msgs::PoseStampedConstPtr &msg);
  void laneCallback(const autoware_msgs::Lane::ConstPtr& msg);

  bool is_wp_subscribed_ = false;
  bool is_current_velocity_subscribed_ =false;
};

CompOptimalVelNode::CompOptimalVelNode()
{
  // pub_waypoints_ = n.advertise<autoware_msgs::Lane>("base_waypoints", 1);
  // pub_waypoints_ = n.advertise<autoware_msgs::Lane>("safety_waypoints", 1);
  pub_waypoints_ = n.advertise<autoware_msgs::Lane>("final_waypoints", 1);
  //sub_pose_ = n.subscribe("current_pose", 1, &CompOptimalVelNode::callbackFromPoseStamped, this);
  sub_vel_ = n.subscribe("current_velocity", 1, &CompOptimalVelNode::callbackFromTwistStamped, this);
  waypoints_sub_ = n.subscribe("safety_waypoints", 1, &CompOptimalVelNode::laneCallback, this);
}
CompOptimalVelNode::~CompOptimalVelNode()
{
}

// void CompOptimalVelNode::publishLaneArray()
// {
//   autoware_msgs::LaneArray array(lane_array_);

//   if (replanning_mode_)
//   {
//     replan(array);
//   }

//   lane_pub_.publish(array);
//   is_first_publish_ = false;
// }
// void CompOptimalVelNode::callbackFromPoseStamped(const geometry_msgs::PoseStampedConstPtr &msg)
// {
//    current_pose_ = *msg;
//   // is_current_pose_subscribed_ = true;

//   // if (current_lane_idx_ == -1)
//   //   initForLaneSelect();
//   // else
//   //   processing();
//   // ROS_WARN_STREAM("pose"<<lane.waypoints[0].pose.pose);
//   //ROS_WARN_STREAM("pose: "<<current_pose_.pose<< " lane_in: "<<lane_in);

//   if (lane_in)
//   {
//     closest_wp = getClosestWaypointNumber(lane, current_pose_.pose,
//                                  current_velocity_.twist, closest_wp,
//                                  distance_threshold, search_closest_waypoint_minimum_dt);
//     ROS_WARN_STREAM("closest_wp: "<<closest_wp);

//     autoware_msgs::Lane safety_waypoints;
//     safety_waypoints.header = lane.header;
//     safety_waypoints.increment = lane.increment;

//     // push waypoints from closest index
//     for (int i = 0; i < safety_waypoints_size_; ++i)
//     {
//       // int index = getLocalClosestWaypoint(current_waypoints, current_pose_global_.pose, closest_search_size_) + i;
//       // if (index < 0 || static_cast<int>(current_waypoints.waypoints.size()) <= index)
//       // {
//       //   break;
//       // }
//       if (closest_wp+i >= lane.waypoints.size())
//         break;
//       const autoware_msgs::Waypoint& wp = lane.waypoints[closest_wp+i];
//       safety_waypoints.waypoints.push_back(wp);
//     }

//     if (safety_waypoints.waypoints.size() > 0)
//     {
//       pub_waypoints_.publish(safety_waypoints);
//       //pub_waypoints_.publish(lane);
//     }

    

//   }


//}

void CompOptimalVelNode::callbackFromTwistStamped(const geometry_msgs::TwistStampedConstPtr &msg)
{
  current_velocity_ = *msg;
  is_current_velocity_subscribed_ = true;

  // if (current_lane_idx_ == -1)
  //   initForLaneSelect();
  // else
  //   processing();
  // ROS_WARN_STREAM("lane"<<lane.waypoints[0].pose.pose);
  // ROS_WARN_STREAM("vel"<<current_velocity_.twist);
}

void CompOptimalVelNode::laneCallback(const autoware_msgs::Lane::ConstPtr& msg)
{
  wp_in_ = *msg;
  is_wp_subscribed_ = true;
  // if (is_current_velocity_subscribed_)
  // {
  //   wp_out_ = compute_time_optimal_vel(wp_in_,current_velocity_); 
  //   pub_waypoints_.publish(wp_out_);
  // }
  wp_out_ = wp_in_;
  pub_waypoints_.publish(wp_out_);

  //detection_range_pub.publish(marker_array);
  //marker_array.markers.clear();
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "comp_optimal_vel");
  CompOptimalVelNode OV;
  ros::spin();

  return 0;
}
