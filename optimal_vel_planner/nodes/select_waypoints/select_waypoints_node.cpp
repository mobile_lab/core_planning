/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <ros/ros.h>
#include <autoware_msgs/LaneArray.h>
#include <std_msgs/Int32.h>
#include <autoware_msgs/VehicleStatus.h>//tmp!!
#include "libwaypoint_follower/libwaypoint_follower.h"
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
//#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>

static const int SYNC_FRAMES = 50;

typedef message_filters::sync_policies::ExactTime<geometry_msgs::TwistStamped, geometry_msgs::PoseStamped>//ApproximateTime
    TwistPoseSync;

geometry_msgs::Point convertPointIntoRelativeCoordinate(const geometry_msgs::Point &input_point,
                                                        const geometry_msgs::Pose &pose)
{
  tf::Transform inverse;
  tf::poseMsgToTF(pose, inverse);
  tf::Transform transform = inverse.inverse();

  tf::Point p;
  pointMsgToTF(input_point, p);
  tf::Point tf_p = transform * p;
  geometry_msgs::Point tf_point_msg;
  pointTFToMsg(tf_p, tf_point_msg);
  return tf_point_msg;
}

double getTwoDimensionalDistance(const geometry_msgs::Point &target1, const geometry_msgs::Point &target2)
{
  double distance = sqrt(pow(target1.x - target2.x, 2) + pow(target1.y - target2.y, 2));
  return distance;
}

// get closest waypoint from current pose
int32_t getClosestWaypointNumber(const autoware_msgs::Lane &current_lane, const geometry_msgs::Pose &current_pose,
                                 const geometry_msgs::Twist &current_velocity, const int32_t previous_number,
                                 const double distance_threshold, const int search_closest_waypoint_minimum_dt)
{
  if (current_lane.waypoints.size() < 2)
    return -1;

  std::vector<uint32_t> idx_vec;
  // if previous number is -1, search closest waypoint from waypoints in front of current pose
  uint32_t range_min = 0;
  uint32_t range_max = current_lane.waypoints.size();
  if (previous_number == -1)
  {
    idx_vec.reserve(current_lane.waypoints.size());
  }
  else
  {
    if (distance_threshold <
        getTwoDimensionalDistance(current_lane.waypoints.at(previous_number).pose.pose.position, current_pose.position))
    {
      ROS_WARN("Current_pose is far away from previous closest waypoint. Initilized...");
      return -1;
    }
    range_min = static_cast<uint32_t>(previous_number);
    double ratio = 3;
    double dt = std::max(current_velocity.linear.x * ratio, static_cast<double>(search_closest_waypoint_minimum_dt));
    if (static_cast<uint32_t>(previous_number + dt) < current_lane.waypoints.size())
    {
      range_max = static_cast<uint32_t>(previous_number + dt);
    }
  }
  const LaneDirection dir = getLaneDirection(current_lane);
  const int sgn = (dir == LaneDirection::Forward) ? 1 : (dir == LaneDirection::Backward) ? -1 : 0;
  for (uint32_t i = range_min; i < range_max; i++)
  {
    geometry_msgs::Point converted_p =
      convertPointIntoRelativeCoordinate(current_lane.waypoints.at(i).pose.pose.position, current_pose);
    double angle = getRelativeAngle(current_lane.waypoints.at(i).pose.pose, current_pose);
    if (converted_p.x * sgn > 0 && angle < 90)
    {
      idx_vec.push_back(i);
    }
  }

  if (idx_vec.empty())
    return -1;

  std::vector<double> dist_vec;
  dist_vec.reserve(idx_vec.size());
  for (const auto &el : idx_vec)
  {
    double dt = getTwoDimensionalDistance(current_pose.position, current_lane.waypoints.at(el).pose.pose.position);
    dist_vec.push_back(dt);
  }
  std::vector<double>::iterator itr = std::min_element(dist_vec.begin(), dist_vec.end());
  int32_t found_number = idx_vec.at(static_cast<uint32_t>(std::distance(dist_vec.begin(), itr)));
  return found_number;
}


class SelectWaypointsNode
{
public:
  SelectWaypointsNode();
  ~SelectWaypointsNode();
private:
  ros::NodeHandle n;
  ros::Publisher pub_waypoints_,pub_closest_waypoint_,pub_vs_;
  
  ros::Subscriber lane_sub_;//,sub_pose_,sub_vel_;
  message_filters::Subscriber<geometry_msgs::TwistStamped> *sub_vel_;
  message_filters::Subscriber<geometry_msgs::PoseStamped> *sub_pose_;
  message_filters::Synchronizer<TwistPoseSync> *sync_tp_;

  autoware_msgs::LaneArray lane_array_;
  autoware_msgs::Lane lane_;
  geometry_msgs::PoseStamped current_pose_;
  geometry_msgs::TwistStamped current_velocity_;
 // void publishLaneArray();
  void callbackFromTwistStamped(const geometry_msgs::TwistStampedConstPtr &msg);
  void callbackFromPoseStamped(const geometry_msgs::PoseStampedConstPtr &msg);

  void TwistPoseCallback(const geometry_msgs::TwistStampedConstPtr &twist_msg,
                         const geometry_msgs::PoseStampedConstPtr &pose_msg);// const;
  void laneCallback(const autoware_msgs::LaneArray::ConstPtr& lane_array);
  //void laneCallback(const autoware_msgs::Lane::ConstPtr& lane);

  bool lane_in = false;
  int32_t closest_wp = -1;
  double distance_threshold = 5;
  double search_closest_waypoint_minimum_dt = 5;
  int safety_waypoints_size_ = 100;

};

SelectWaypointsNode::SelectWaypointsNode()
{
  // pub_waypoints_ = n.advertise<autoware_msgs::Lane>("base_waypoints", 1);
  //pub_waypoints_ = n.advertise<autoware_msgs::Lane>("safety_waypoints", 1);
  pub_waypoints_ = n.advertise<autoware_msgs::Lane>("final_waypoints", 1);
  pub_closest_waypoint_ = n.advertise<std_msgs::Int32>("closest_waypoint", 1);//not needed
  pub_vs_ = n.advertise<autoware_msgs::VehicleStatus>("vehicle_status", 1);//tmp!!
  // sub_pose_ = n.subscribe("current_pose", 1, &SelectWaypointsNode::callbackFromPoseStamped, this);
  // sub_vel_ = n.subscribe("current_velocity", 1, &SelectWaypointsNode::callbackFromTwistStamped, this);
  
  //lane_sub_ = n.subscribe("trajectory_waypoints", 1, &SelectWaypointsNode::laneCallback, this);
  //lane_sub_ = n.subscribe("/based/lane_waypoints_raw", 1, &SelectWaypointsNode::laneCallback, this);
  lane_sub_ = n.subscribe("lane_waypoints_array", 1, &SelectWaypointsNode::laneCallback, this);


  sub_pose_ = new message_filters::Subscriber<geometry_msgs::PoseStamped>(n, "current_pose_slow", 50);
  sub_vel_ = new message_filters::Subscriber<geometry_msgs::TwistStamped>(n, "current_velocity_slow", 50);
  sync_tp_ = new message_filters::Synchronizer<TwistPoseSync>(TwistPoseSync(SYNC_FRAMES), *sub_vel_, *sub_pose_);
  sync_tp_->registerCallback(boost::bind(&SelectWaypointsNode::TwistPoseCallback, this, _1, _2));
}
SelectWaypointsNode::~SelectWaypointsNode()
{
}

void SelectWaypointsNode::TwistPoseCallback(const geometry_msgs::TwistStampedConstPtr &twist_msg,
                                      const geometry_msgs::PoseStampedConstPtr &pose_msg) //const
{
  //ROS_WARN_STREAM("SelectWaypointsNode pose: "<<pose_msg->pose);


if (lane_in)
  {
    closest_wp = getClosestWaypointNumber(lane_, pose_msg->pose,
                                 twist_msg->twist, closest_wp,
                                 distance_threshold, search_closest_waypoint_minimum_dt);
   // ROS_WARN_STREAM("closest_wp: "<<closest_wp);
  pub_closest_waypoint_.publish(closest_wp);

  autoware_msgs::VehicleStatus vs;
  vs.speed = 0.0;
  vs.angle = 0.0;
  pub_vs_.publish(vs);//tmp!!
    autoware_msgs::Lane safety_waypoints;
    safety_waypoints.header = pose_msg->header;//lane.header;
    safety_waypoints.increment = lane_.increment;

    // push waypoints from closest index
    for (int i = 0; i < safety_waypoints_size_; ++i)
    {
      // int index = getLocalClosestWaypoint(current_waypoints, current_pose_global_.pose, closest_search_size_) + i;
      // if (index < 0 || static_cast<int>(current_waypoints.waypoints.size()) <= index)
      // {
      //   break;
      // }
      if (closest_wp+i >= lane_.waypoints.size())
        break;
      const autoware_msgs::Waypoint& wp = lane_.waypoints[closest_wp+i];
      safety_waypoints.waypoints.push_back(wp);
    }

    if (safety_waypoints.waypoints.size() > 0)
    {
      pub_waypoints_.publish(safety_waypoints);
      //pub_waypoints_.publish(lane);
    }

  }
}


//void SelectWaypointsNode::laneCallback(const autoware_msgs::Lane::ConstPtr& lane)
void SelectWaypointsNode::laneCallback(const autoware_msgs::LaneArray::ConstPtr& lane_array)
{
  lane_array_ = *lane_array;
  lane_ = lane_array_.lanes[0];
  //lane_ = *lane;
  lane_in = true;

  
  
  //ROS_WARN_STREAM("lane"<<lane.waypoints[0].pose.pose);
  //publishLaneArray();
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "select_waypoints");
  SelectWaypointsNode SW;
  ros::spin();

  return 0;
}
