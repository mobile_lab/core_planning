// c_lib.cpp : Defines the exported functions for the DLL application.
//

//#include "stdafx.h"
#include "optimal_vel_lib.h"
#include <iostream>
#include <fstream>
#include <cmath>    
#include <ros/ros.h>
#define SQR(x) ((x)*(x))
using namespace std;

// extern "C"
// {

	void diff2(float **p, float s[], float **ps, float **pss, int jp);
	struct VehicleData
	{
		float f_max;
		float sdmax;
		float mass;
		float friction_coefficient;
		float height;
		float width;
	};
	class Path
	{
	public:
		int lenght;
		float** pxyz;
		float* s;
		float **Ps;
		float **Pss;

		void computePathParameters()
		{

			diff2(pxyz, s, Ps, Pss, lenght);
		}
		Path(int lenght_in, float* x, float* y, float* z)
		{
			lenght = lenght_in;
			pxyz = new float*[lenght];
			Ps = new float*[lenght];
			Pss = new float*[lenght];
			for (int i = 0; i < lenght; i++)
			{
				pxyz[i] = new float[3];
				Ps[i] = new float[3];
				Pss[i] = new float[3];
			}
			s = new float[lenght];

			for (int i = 0; i < lenght; i++)
			{
				pxyz[i][0] = x[i];
				pxyz[i][1] = y[i];
				pxyz[i][2] = z[i];
			}
			computePathParameters();
		}
		~Path()
		{
			//ROS_WARN_STREAM("here");
			for(int i = 0; i < lenght; ++i) {
				delete[] pxyz[i];   
			}
			delete[] pxyz;
			for(int i = 0; i < lenght; ++i) {
				delete[] Ps[i];   
			}
			delete[] Ps;
			for(int i = 0; i < lenght; ++i) {
				delete[] Pss[i];   
			}
			delete[] Pss;

			delete[] s;
		}

	};
	/*************************************************/
	/* compute  S,Xs,Xss by finite difference method */
	/* p  : position vector                          */
	/* s  : path length                              */
	/* ps : path tangent vector                      */
	/* pss: path normal                              */
	/* jp : number of points                         */
	/*************************************************/
	void diff2(float **p, float *s, float **ps, float **pss, int jp)
	{
		int i, j;
		double bb, ds;
		double dis;


		s[0] = 0.;
		for (i = 1; i<jp; i++)
		{
			bb = 0.;
			for (j = 0; j<3; j++)
				bb += (p[i][j] - p[i - 1][j])*(p[i][j] - p[i - 1][j]);

			if (bb > 0)
				ds = sqrt(bb);
			else
			{
				printf("bb<0 in diff2\n");
				return;
				ds = 0.000001;
			}
			s[i] = s[i - 1] + (float)ds;
		}



		for (i = 0; i<jp - 1; i++)
		{
			dis = (s[i + 1] - s[i]);
			if (abs(dis)<0.0000001)
			{
				printf("error - dis = 0 in diff2\n");

				return;
			}
			for (j = 0; j<3; j++)
			{

				ps[i][j] = (p[i + 1][j] - p[i][j]) / dis;
			}
		}


		/* last point */
		for (j = 0; j<3; j++)
			ps[jp - 1][j] = ps[jp - 2][j];




		for (i = 0; i<jp - 2; i++)
		{
			//for(i=1;i<jp;i++)//changed differantion from (i+1) - i to i - (i-1)  gavriel 21.8.16
			dis = (s[i + 1] - s[i]);
			//dis = ((s[i]-s[i-1]));// +  (s[i+1]-s[i])) / 2 ;
			if (abs(dis)<0.0000001)
			{
				printf("error - dis = 0 in diff2\n");
				return;
			}
			for (j = 0; j<3; j++)
			{

				pss[i][j] = (ps[i + 1][j] - ps[i][j]) / dis;
				//pss[i][j]=(ps[i][j]-ps[i-1][j])/dis;
			}
		}
		//  //first point
		//for(j=0;j<3;j++)
		//  pss[0][j]=pss[1][j];
		/* last two points */
		for (j = 0; j<3; j++)
			pss[jp - 1][j] = pss[jp - 2][j] = pss[jp - 3][j];

		/*
		for(i=0;i<jp-1;i++)
		{
		printf("s: %d %f \n",i,s[i]);
		printf("p: %f %f %f \n",p[i][0],p[i][1],p[i][2]);
		printf("ps: %f %f %f \n",ps[i][0],ps[i][1],ps[i][2]);

		printf("pss: %f %f %f \n",pss[i][0],pss[i][1],pss[i][2]);
		dot_product(ps[i],pss[i],&psss);
		printf("ps*Pss %f \n",psss);
		}
		*/
	}
	/******************************************************/
	/* this subroutine is to calculate the cross product  */
	/* cc= aa*bb                                          */
	/******************************************************/

	void cross_product(float aa[], float bb[], float cc[])
	{
		cc[0] = aa[1] * bb[2] - aa[2] * bb[1];
		cc[1] = aa[2] * bb[0] - aa[0] * bb[2];
		cc[2] = aa[0] * bb[1] - aa[1] * bb[0];
		return;
	}

	/******************************************************/
	/* this subroutine is to calculate the  dot  product  */
	/* cc= aa.bb                                          */
	/******************************************************/

	void dot_product(float aa[], float bb[], float *cc)
	{
		int i;
		*cc = 0.;
		for (i = 0; i<3; i++)
			*cc += aa[i] * bb[i];
		return;
	}

	/*THIS SUBROUTINE TO FIND THE SQARE OF A MATRIX */
	void sqarmatrix(float m[], int *n, float *z)
	{
		int i;

		*z = 0.;
		for (i = 0; i<*n; i++)
			*z += (m[i] * m[i]);
		return;
	}


	/*************************************************/
	/* this subroutine is use to normalize a vector  */
	/* m: vector to be normalized                    */
	/* n: dimension of the vector                    */
	/*************************************************/

	void normalize(float m[], int n)
	{
		double zz;
		float  z;
		int j;

		sqarmatrix(m, &n, &z);

		if (z < 1.e-20)
		{
			zz = sqrt(1. / (double)n);
			for (j = 0; j<3; j++)
				m[j] = (float)zz;
		}
		else
		{
			zz = (double)z;
			zz = sqrt(zz);
			for (j = 0; j<3; j++)
				m[j] = m[j] / (float)zz;
		}
		return;
	}
	float slide(float ai, float bi, float ci, float sdmax)
		//float ai,bi,ci;
	{
		float aa, bb;
		int id;
		float ub, lb, temp, roots, rootb, sdl1;


		aa = abs(ai);
		bb = abs(bi);

		if (aa < 1.e-10)
		{
			if (bb < 1.e-10)
			{
				if (ci >= 0.0f)
					id = 1;
				else
					id = 2;
			}
			else if (bi > 0.0f)
			{
				if ((bi*ci) >= 0.0f)
					id = 1;
				else
				{
					id = 3;
					ub = sdmax;
					lb = sqrt(ci / (bi*(-2.0f)));
				}
			}
			else
			{
				if ((bi*ci) >= 0.0f)
					id = 2;
				else
				{
					id = 3;
					lb = 0.0f;
					ub = sqrt(ci / (bi*(-2.0f)));
					if (ub > sdmax)
						ub = sdmax;
				}
			}
		}
		else
		{

			if ((bi*bi - ai*ci) < 0.0f)
			{
				if (ai > 0.0f)
					id = 1;
				else
					id = 2;
			}
			else
			{
				if (ai > 0.0f)
				{
					temp = sqrt(bi*bi - ai*ci);
					roots = (bi + temp) / (ai*(-1.0f));
					rootb = (temp - bi) / ai;

					if (rootb < 0.0f)
						id = 1;
					else if (roots > 0.0f)
					{
						id = 4;
						lb = sqrt(roots);
						ub = sqrt(rootb);
					}
					else if ((roots < 0.0f) && (rootb > 0.0f))
					{
						lb = sqrt(rootb);
						if (lb > sdmax) id = 2;
						else
						{
							id = 3;
							ub = sdmax;
						}
					}
					else
					{
						printf("logical error !\n");
						return (0.0f);
					}
				}
				else
				{
					temp = sqrt(bi*bi - ai*ci);
					rootb = (bi + temp) / (ai*(-1.0f));
					roots = (temp - bi) / ai;
					if (rootb < 0.0f)
						id = 2;
					else if (roots > 0.0f)
					{
						id = 3;
						lb = sqrt(roots);
						ub = sqrt(rootb);
					}
					else if ((roots < 0.0f) && (rootb > 0.0f))
					{
						id = 3;
						lb = 0.0f;
						ub = sqrt(rootb);
					}
					else
					{
						printf("logical error !\n");
						return (0.0f);
					}
				}
			}
		}

		switch (id)
		{
		case 1:
			sdl1 = sdmax;
			break;
		case 2:
			/* *(dead + ndead++)=i;*/
			sdl1 = 0.;
			/* path_error=1; */
			break;
		case 3:
			if (ub > sdmax)
				ub = sdmax;
			if (lb > sdmax)
				lb = sdmax;
			/* feb-15-89 recorrect*/
			if (lb> 0.000001f)
			{
				sdl1 = 0.0f;
				/* *(dead + ndead++)=i;*/
			}
			else
				sdl1 = ub;     /* need more check ! */
			break;
		case 4:
			if (ub > sdmax)
				ub = sdmax;
			if (lb > sdmax)
				lb = sdmax;
			sdl1 = lb;
			break;

		default:
			printf("id error! id=%d \n", id);
			break;
		}

		return ((float)sdl1);
	}

	float contact(float xss, float rk, float rn, float sdmax)
	{
		float sd;
		float g = 9.81;
		//extern float g, sdmax;

		if (abs((rn*xss))<.000001)
			return sdmax;
		sd = (float)((-g*rk) / xss / rn);
		if (sd<0)
			return sdmax;
		sd = (float)(sqrt(sd));
		if (sd>sdmax)
			sd = sdmax;
		return sd;
	}
	float tip_over(float a, float b, float c, float sdmax)
	{
		float x, sx, r1, r2, r;

		if (abs(a)<1e-8)
			if (abs(b)<1e-8)
				if (c <= 0)
				{
					return sdmax;
				}
				else
				{
					return 0.;
				}
			else
			{
				x = (float)(-c / 2. / b);
				if (b>0)
					if (x<0)
						return 0.;
					else
					{
						sx = (float)(sqrt(x));
						return (sdmax>sx ? sx : sdmax);
					}
				else
					if (x <= 0)
						return sdmax;
					else
						return 0;
			}
		else
		{
			x = (float)(b*b - a*c);
			if (x<0)
				if (a>0)
					return 0;
				else
					return sdmax;
			else
			{
				x = (float)(sqrt(x));
				r1 = (float)((-b + x) / a);
				r2 = (float)((-b - x) / a);
				if (a>0)
					if (r1 <= 0)
						return 0;
					else
					{
						r = (float)(sqrt(r1));
						if (r2 <= 0)
							return (sdmax>r ? r : sdmax);
						else
							return 0;
					}
				else
					if (r2 <= 0)
						return sdmax;
					else
						if (r1 <= 0)
							return 0;
						else
						{
							r = (float)(sqrt(r1));
							return (sdmax>r ? r : sdmax);
						}
			}
		}
	}

	/*******************************************************************/
	/* this subroutine is to find the function of the B-spline surface */
	/* P = U*M*R*M*W  : position vector                                */
	/* pu= tangent vector along u direction                            */
	/* pw= tangent vector along w direction                            */
	/* M = trandformation matrix                                       */
	/* conp = control point                                            */
	/* s,t =  to indentify a particular patch in surface               */
	/*******************************************************************/

	/*bspf2(u,w,p,pu,pw)
	float u,w;
	float p[3],pu[3],pw[3];*/
	//void bspf2(float u, float w, float p[3], float pu[3], float pw[3])
	//{
	//	static float m[4][4] = {
	//		{ -1.f / 6.f, 3.f / 6.f,-3.f / 6.f, 1.f / 6.f },
	//		{ 3.f / 6.f,-6.f / 6.f, 3.f / 6.f,     0.f },
	//		{ -3.f / 6.f,     0.f, 3.f / 6.f,     0.f },
	//		{ 1.f / 6.f, 4.f / 6.f, 1.f / 6.f,     0.f }
	//	};
	//
	//	float tt, uu, ww, cc;
	//	float du[4], dw[4], uv[4], wv[4], r[4][4], mt[4][4], mr[4][4], mrm[4][4];
	//	int ns, nt, i, j, k;
	//	if (u < 0)
	//	{
	//		printf("error - u<0 in bspf2\n");//unknwon error cause u<0
	//		return;
	//	}
	//	uu = u;
	//	ww = w;
	//	ns = u;
	//	nt = w;
	//	u = u - ns;
	//	w = w - nt;
	//	uv[3] = 1.;
	//	uv[2] = u;
	//	uv[1] = u * u;
	//	uv[0] = uv[1] * u;
	//
	//	wv[3] = 1.;
	//	wv[2] = w;
	//	wv[1] = w * w;
	//	wv[0] = wv[1] * w;
	//
	//	du[3] = 0.;
	//	du[2] = 1.;
	//	du[1] = 2.*u;
	//	du[0] = 3.*u*u;
	//
	//	dw[3] = 0.;
	//	dw[2] = 1.;
	//	dw[1] = 2.*w;
	//	dw[0] = 3.*w*w;
	//
	//
	//	/* transport of transformation matrix  */
	//	for (i = 0; i<4; i++)
	//		for (j = 0; j<4; j++)
	//			mt[i][j] = m[j][i];
	//	for (k = 0; k<3; k++)
	//	{
	//		for (i = 0; i<4; i++)
	//			for (j = 0; j<4; j++)
	//			{
	//				r[j][i] = conp[i + nt][j + ns][k];
	//			}
	//
	//
	//		mam4(m, r, mr);
	//		mam4(mr, mt, mrm);
	//		p[k] = pw[k] = pu[k] = 0.;
	//		for (j = 0; j <4; j++)
	//		{
	//			cc = tt = 0.;
	//			for (i = 0; i<4; i++)
	//			{
	//				tt += uv[i] * mrm[i][j];
	//				cc += du[i] * mrm[i][j];
	//			}
	//
	//			p[k] += tt*wv[j];
	//
	//
	//			pw[k] += tt*dw[j];
	//			pu[k] += cc*wv[j];
	//		}
	//
	//	}
	//	//if(p[0] == 0)
	//	//		printf("temp error\n");
	//	return;
	//}
	void kinematics(float** pxyz, float** ps, int jp, float* s, float** r, float** fp, float** ft)
	{
		float** p = new float*[jp];
		for (int i = 0; i < jp; i++)
			p[i] = new float[2];

		float pu[3], pw[3], pp[3];
		float vr[3], vt[3], vp[3];/*vk[3],vn[3];*/

								  /*float xss, a[200],b[200],c[200],d[200],root1,root2,rn,rk,pk,pn,tk,temp;*/
		int i, j, mn = 3;
		//extern float mfac[1000];


		//convert(pa, 2);

		//bspline(&jp, p, ier);//create path from conpc

		for (i = 0; i<jp; i++)
		{
			//bspf2(p[i][0], p[i][1], pp, pu, pw);

			///*normal vector to surface vr:*/
			//cross_product(pu, pw, vr);
			vr[0] = 0;//flat surface
			vr[1] = 0;
			vr[2] = 1;
			normalize(vr, 3);
			/*printf("%d %f %f %f",i,vr[0],vr[1],vr[2]);*/
			for (j = 0; j<3; j++)
			{
				//pxyz[i][j] = pp[j];
				//if(pxyz[i][0] == 0 || (pp[0] == 0))
				//	printf("temp error\n");

				r[i][j] = vr[j];
			}
		}

		//diff2(pxyz, s, ps, pss, jp);
		for (i = 0; i<jp; i++)
		{
			for (j = 0; j<3; j++)
			{
				pp[j] = pxyz[i][j];
				vr[j] = r[i][j];
				vt[j] = ps[i][j];
			}
			cross_product(vr, vt, vp);
			normalize(vp, 3); /* make unit vector feb-15-89 */
			normalize(vt, 3);

			for (j = 0; j<3; j++)
			{
				fp[i][j] = vp[j];
				ft[i][j] = vt[j];
			}
		}
		for(int i = 0; i < jp; ++i) 
			delete[] p[i];   
		delete[] p;
		return;
	}
	void limitc(Path & path, VehicleData data, float* sdl, float a_slide[], float b_slide[], float c_slide[], float gkt[])
	{
		//copy values to local variables
		int jp = path.lenght;
		float f_max = data.f_max;
		float sdmax = data.sdmax;
		float mass = data.mass;
		float fc = data.friction_coefficient;
		float hb = data.height / (data.width / 2);
		///////////////////////////////

		float g = 9.81;

		float** r = new float*[jp];
		float** fp = new float*[jp];
		float** ft = new float*[jp];
		for (int i = 0; i < jp; i++)
		{
			r[i] = new float[3];
			fp[i] = new float[3];
			ft[i] = new float[3];
		}
		float* xss = new float[jp];
		float* pn = new float[jp];
		float* pk = new float[jp];
		//float* gkt = new float[jp];
		float* mfac = new float[jp];
		for (int i = 0; i < jp; i++)
		{
			mfac[i] = 1;
		}
		kinematics(path.pxyz, path.Ps, jp, path.s, r, fp, ft);






		int path_error_geo = 0;
		int path_error_dyn = 0;
		//////////////////////
		//float a_slide[1000], b_slide[1000], c_slide[1000];
		float* a_tip = new float[jp];
		float* b_tip = new float[jp];
		float* c_tip = new float[jp];
		float* sdl_slide = new float[jp];
		float* sdl_tip = new float[jp];
		float* sdl_cont = new float[jp];
		float* sdl_engine = new float[jp];


		int ndead; // 7-11-2015 defined again shiller

		float vr[3], vn[3], vt[3], vp[3], vk[3], vl[6]; // 7-11-2015 shiller increased vl to 4: slide, tip, contat, engine. 8.5.16 gavriel increased vl to 6: slide, tip, contact, engine, max wheels velocities, diferention of radius of curvature
		float rk, rn, tk;
		//float sddl_h[200],sddl_l[200];
		int i, j, mn = 3;
		float mf;
		//FILE *ofp;
		float tmp;
		float alpha;      
		float* alphav =  new float[jp];             /* 8-28-90 steering angle. */
		                   /* 5-2-94 steering angle vector. */
												/*float fff,maxfff;   */             /* 9-2-90 for checking force limit. */


		 // path for longitudinal limits 30-8-06 shiller
		float** pxz = new float*[jp];
		for (int i = 0; i < jp; i++)
			pxz[i] = new float[2];

		float aa, bb, dx, sin_alfa, cos_alfa;
		float width = 0.305;// robot width gavriel 3.5.16
		float cur_rad;
		float ang_acc; //angular accelaration
		float tmp1;
		float* lim_curve2 =  new float[jp]; 
		float* lim_curve3 =  new float[jp]; 

		// compute path for longitudinal limits 30-8-06 shiller
		//  pxz[0] = + sqrt(dx^2+dy^2) length of path on xy plane
		//  pxz[1] = pxyz[2]  height same as original path 

		// velocity limit due to engine and frictin constraint--longitudinal model
		bb = f_max; //2*mass*g;  // parameters of force-v equation
		aa = bb / (0.3*sdmax);

		pxz[0][1] = path.pxyz[0][2];
		pxz[0][0] = 0;
		sdl_engine[0] = sdmax;
		for (i = 1; i<jp; i++)
		{
			pxz[i][1] = path.pxyz[i][2];  // z coordinate 
			dx = sqrt(SQR(path.pxyz[i][0] - path.pxyz[i - 1][0]) + SQR(path.pxyz[i][1] - path.pxyz[i - 1][1]));  // incremental distance along path in x and z 
			pxz[i][0] = pxz[i - 1][0] + dx;  // create the next point 
											 // compute slope
			sin_alfa = (pxz[i][1] - pxz[i - 1][1]) / sqrt(SQR(dx) + SQR(pxz[i][1] - pxz[i - 1][1]));
			cos_alfa = sqrt(1 - SQR(sin_alfa));

			// penalize velocity limit due to engine only when slope is too steep shiller 25/1/2016 		
			sdl_engine[i] = sdmax;
			if (mass*g*sin_alfa > mass*g*fc*cos_alfa || mass*g*sin_alfa > f_max)
			{
				sdl_engine[i] = 0;  // reduce  velocity limit if engine cannot overcome gravity
			}
		}


		path_error_geo = 0;
		path_error_dyn = 0;

		/* fi=fopen("abcd.dat","w"); */
		ndead = 0;
		/* define vector vk */
		vk[0] = vk[1] = 0.;
		vk[2] = 1.;

		for (i = 0; i<jp; i++)
		{
			for (j = 0; j<3; j++)
			{
				vr[j] = r[i][j];   // unit vectors
				vp[j] = fp[i][j];
				vt[j] = ft[i][j];
				vn[j] = path.Pss[i][j];  // vn is normalized later
			}

			sqarmatrix(vn, &mn, &xss[i]);
			xss[i] = sqrt((double)xss[i]);

			normalize(vn, 3);

			/*calculate a,b,c,d to find the limit curve*/
			dot_product(vr, vn, &rn);
			dot_product(vp, vn, &pn[i]);
			dot_product(vr, vk, &rk);
			dot_product(vp, vk, &pk[i]);
			dot_product(vt, vk, &tk);

			/* if path geometrically feasible, compute velocity limit. */
			if (!path_error_geo)
			{
				/* compute coefficients for eqs of velocity limit. */
				if (mfac[i]<1.)
				{
					mf = mfac[i] * mfac[i];
					a_slide[i] = xss[i] * xss[i] * (rn*rn*fc*fc*mf - pn[i] * pn[i]);
					b_slide[i] = g*xss[i] * (fc*fc*rk*rn*mf - pk[i] * pn[i]);
					c_slide[i] = g*g*(fc*fc*rk*rk*mf - pk[i] * pk[i]);
					gkt[i] = g*tk;
				}
				else
				{
					a_slide[i] = xss[i] * xss[i] * (rn*rn*fc*fc - pn[i] * pn[i]);
					b_slide[i] = g*xss[i] * (fc*fc*rk*rn - pk[i] * pn[i]);
					c_slide[i] = g*g*(fc*fc*rk*rk - pk[i] * pk[i]);
					gkt[i] = g*tk;
				}

				/* coefficients for tip over constraints equation. */
				a_tip[i] = xss[i] * xss[i] * (pn[i] * pn[i] - rn*rn*hb*hb);
				b_tip[i] = g*xss[i] * (pk[i] * pn[i] - hb*hb*rk*rn);
				c_tip[i] = g*g*(pk[i] * pk[i] - hb*hb*rk*rk);

				/* compute velocity limit from different constraints. */
				if (mfac[i]<1e-3)
					vl[0] = sdl_slide[i] = 0.;
				else
				{
					//if(a_slide[i] <1)

					vl[0] = sdl_slide[i] = slide(a_slide[i], b_slide[i], c_slide[i], sdmax);
					/*	else
					printf("error");*/
				}
				vl[1] = sdl_cont[i] = contact(xss[i], rk, rn, sdmax);
				vl[2] = sdl_tip[i] = tip_over(a_tip[i], b_tip[i], c_tip[i], sdmax);
				vl[3] = sdl_engine[i]; // 7-11-2015 shiller 

				for (sdl[i] = vl[0], j = 1; j<4; j++) // 7-11-2015 shiller increased index to 4. gavriel 8.5.16 increased index to 6
					sdl[i] = (sdl[i]>vl[j] ? vl[j] : sdl[i]);

				// fprintf(fi,"%f %f \n",vl[5],sdl[i]);
				if (sdl[i] < 0.000001)
				{
					////*(dead + ndead++)=i;
					//ndead++;  // 7-11-2015 shiller simpified. no need for dead
					ndead = ndead + 1;  // 7-11-2015 shiller simpified. no need for dead
										//	printf("sdl_zero_flag = 1");
				}
			} /* for */
		} /* for */




		  /* Signal when dead points are encountered. */
		if (ndead != 0)
		{
			path_error_dyn = ndead;
		}



		for(int i = 0; i < jp; ++i)
			delete[] r[i];   
		delete[] r;
		for(int i = 0; i < jp; ++i)
			delete[] fp[i];   
		delete[] fp;
		for(int i = 0; i < jp; ++i)
			delete[] ft[i];   
		delete[] ft;

		delete[] xss;
		delete[] pn;
		delete[] pk;
		delete[] mfac;

		delete[] a_tip;
		delete[] b_tip;
		delete[] c_tip;
		delete[] sdl_slide;
		delete[] sdl_tip;
		delete[] sdl_cont;
		delete[] sdl_engine;

		delete[] alphav;

		for(int i = 0; i < jp; ++i)
			delete[] pxz[i];   
		delete[] pxz;
		delete[] lim_curve2;
		delete[] lim_curve3;


		return;
	}






	//////////////////////////////////////////////////

	void accl_(int index, float sd, VehicleData data, float a_slide, float b_slide, float c_slide, float gkt, float *acchigh, float *acclow)
	{
		//int index;
		//float sd, sdli;
		float sd2, square, accmax, accmin, accmax_tmp, accmin_tmp;
		float aa, bb, sin_alfa, cos_alfa, dx;
		float accmax_engine, acclow_engine;
		//int i;
		float cur_rad;//gavriel 3.5.16

					  //sd = *sdp;
					  //if(override_flag == 0)
					  //{
					  //	model2_acc(&accmax,&accmin,sd,index);
					  //	
					  //}
					  //else
					  //{
					  //	accmax=f_max/mass;
					  //	accmin=f_min/mass;
					  //}
					  //*acchigh = accmax;
					  //*acclow = accmin;
					  //return;


		*acchigh = -1000.;
		*acclow = 1000.;
		//index= *indexp;  // 5-8-06 shiller

		/////sdli = sdl[index];

		sd2 = sd * sd;
		square = (float)(a_slide * sd2*sd2 + 2.*b_slide * sd2 + c_slide);
		//if (ABS((square))<1e-5)
		if (abs((square))<1e-3)//gavriel 6.3.17, at big distances, cause error if < 1e-5
			square = 0.;
		else if (square < 0)
		{
			printf("error square < 0\n");
			return;
		}
		square = (float)(sqrt(square));
		accmax = data.f_max / data.mass;
		accmin = -data.f_max / data.mass;
		//gavriel 3.5.16 accleration when every wheel not cross the maximum accelaration limit
		//if(xss < 0) xss[index] = - xss[index];

		//model2_acc(&accmax,&accmin,sd,index);
		//
		*acchigh = ((accmax > square) ? square : accmax) - gkt;      //if accmax < 0 or accmin >0 this is not good
		//*acchigh = ((accmax < -square) ? -square : accmax) - gkt;   //gavriel 30.5.16

		*acclow = ((accmin >(-square)) ? accmin : (-square)) - gkt;
		//*acclow = ((accmin <(square)) ? accmin : (square)) - gkt;//gavriel 30.5.16


		return;
	}


	// 12-16-91 intf integrates forward one increment

	void intf(float sd1, float ds, float acc, float *sd2, double *dt, int *ier)
	{
		double v2, tmp;


		*ier = 0;		// flag for integration problems
						////if(abs(acc) < 1.e-5 && sd1 > 1.e-7) 

		if (fabs(acc) < 1.e-5 && sd1 > 1.e-7)
		{
			*dt = ((double)ds) / ((double)sd1);
			*sd2 = sd1;
		}
		else
		{

			v2 = (double)(pow(sd1, 2) + 2 * acc*ds);
			if (v2 < 0.) //acc sometimes <0 and this cause v2<0 gavriel 30.5.16
			{
				v2 = 0.;  // leave it now.  6-11-2015 shiller shouldnt reset to 0.  
				*ier = 1;
				////return; 
			}


			tmp = sqrt(v2);
			// if(index < jp)

			// else
			{
				*dt = (tmp - (double)sd1) / ((double)acc);
				*sd2 = (float)tmp;
			}

		}


		return;

	}


	// intgf integrate forward time optimal trajectory
	//void intgf(int ist,int in, double t[],int *ih, int *ih_posture)
	////void intgf(int ist,int *jp, int *ih)  // 31-3-06 shiller
	int intgf(int *ist, int jp, int *ihh, float s[], float sd[], float sdl[], float t[], float acc_vec[], float sdinit, VehicleData data, float a_slide[], float b_slide[], float c_slide[], float gkt[])  // 31-3-06 shiller  6-11-2015 shiller added ier 
	{
		double dt;
		float acc, dec;
		int i;
		float sd1;
		float ds;
		float sd2;
		int ier;
		int int_flag = 0;
		int int_error; // 6-12-2015 shiller
		double ds1, ds2;//gavriel 5.5.16
		float tmp_acc;//
					  //	int ihh; 
					  //int ist, ih, jp;
		if (*ist == 0)
			sd[*ist] = sdinit;
		//else		// start from tangency point (on limit curve)
		//{
		//  if(*ih_posture !=1) 
		//	  sd[ist]=sdl[ist] - 0.1;
		//}

		//*ih_posture = 0;

		i = *ist;

		// start loop

		while (int_flag == 0)
		{
			sd1 = sd[i];
			ds = s[i + 1] - s[i];

			//accl(i,&sd[i],&acc,&dec);//cancled gavriel 3.5.16
			accl_(i, sd[i], data, a_slide[i], b_slide[i], c_slide[i], gkt[i], &acc, &dec);
			if ((acc - dec)  < -0.001)        // < 0: cross limit curve
			{
				////*ih = i;
				*ihh = i;
				//sd[i] = sd[i];//-.1; // 4-8-06 shiller.//cancel .1 gavriel 17.5.16

				//	printf("missed a hit point in intgf %d\n" , i);
				return 0;
			}
			intf(sd1, ds, acc, &sd2, &dt, &ier);
			acc_vec[i] = acc;
			//sdd_vec[i] = acc;//gavriel 9.8.16

	
			if (ier == 1) // integration error (negative velocity) 6-11-2015 shiller
			{
				//	dt = 10; // infinity
				//	sd[i+1] = sd2;
				//	t[i] = dt;
				//return(1);
				int_error = 1; // 6-11-2015 shiller

				return int_error;  // 6-11-2015 shiller stop integration if velocity negative 

			}

			//if(posture[i+1] == 1) 
			//{
			//	if(sd2 > sdmax_side) sd2 = sdmax_side;
			//	if(posture[i+1] != posture[i])
			//	{
			//		*ih_posture = 1;
			//		*ih = i;
			//		sd[i+1] = sd2;
			//		t[i] = (sd[i+1]-sd[i])/(s[i+1]-s[i]);
			//		return;
			//	}
			//}
			//if(posture[i+1] == 2) 
			//{
			//	if(sd2 > sdmax_crwl) sd2 = sdmax_crwl;
			//	if(posture[i+1] != posture[i])
			//	{
			//		*ih_posture = 1;
			//		*ih = i;
			//		sd[i+1] = sd2;
			//		t[i] = (sd[i+1]-sd[i])/(s[i+1]-s[i]);

			//		return;
			//	}
			//}

			if (sd2 > sdl[i + 1]) 	// hit limit curve
			{

				//gavriel 2.5.16 - if integrated velocity cross limit curve - try move at limit curve speed - if the limit curve is real, it is not possible. 
				sd2 = sdl[i + 1];//-0.1;
				tmp_acc = (pow(sd2, 2) - pow(sd1, 2)) / (2 * ds);//find new acc 
				if (tmp_acc < acc && tmp_acc> dec)
				{
					//ds1 = (pow(sd2,2) - pow(sd1,2))/(2*acc);//distance to limit curve cross 
					//ds2 = ds - ds1;//distance from limit curve cross to next point
					//dt = ((double) ds2)/((double) sd2) + (sd2  - (double) sd1)/((double) acc);// - it is more accurate to split the distance, but if no new point is added, this is wrong
					if (tmp_acc > 0.0001)
						dt = (sd2 - (double)sd1) / ((double)tmp_acc);
					else
						dt = ((double)ds) / ((double)sd2);
					acc_vec[i] = tmp_acc;
				}
				else
				{
					////*ih = i;
					*ihh = i + 1;  // 4-8-06 shiller
					sd[i + 1] = sdl[i + 1];//-.1; // 4-8-06 shiller.//cancel .1 gavriel 17.5.16
					acc_vec[i] = tmp_acc;					   //sdd_vec[i] = tmp_acc;//gavriel 9.8.16
					return 0;
				}
			}

			sd[i + 1] = sd2;
			t[i] = dt;

			////if(i == *jp-2) 
			if (i == jp - 2)
			{
				////*ih =*jp;
				*ihh = jp;

				return 0;
			}

			i++;
		}

		return 0;

	}



	// 12-17-91 intb integrates backwards one increment

	void intb(float sd1, float  ds, float dec, float  *sd2, double *dt, int *ier)
	{

		double  v2, tmp;

		*ier = 0;		// flag for integration problems
		if (fabs(dec) < 1.e-3)
		{
			*dt = (double)ds / ((double)sd1);
			*sd2 = sd1;
		}
		else
		{
			v2 = (double)(pow(sd1, 2) - 2 * dec*ds);
			if (v2 < 0.) v2 = 0.;
			tmp = sqrt(v2);
			// if(index < jp)


			*dt = (-tmp + (double)sd1) / ((double)dec);
			*sd2 = (float)tmp;

		}


		return;
	}




	// icr is the crssing point

	//void intgb(int ist,int in,double t[],int *icr)
	////void intgb(int ist,int *jp,int *icr) // 31-3-06 shiller
	void intgb(int *ist, int jp, int *icr, float s[], float sd[], float sdl[], float t[], float acc_vec[], float sdinit, float sdfinal, VehicleData data, float a_slide[], float b_slide[], float c_slide[], float gkt[]) // 31-3-06 shiller
	{

		//	int  ih;                  // tangential point
		double  dt;
		float acc, dec, acca;

		int i;
		float sd1;
		float ds;
		float sd2;
		int ier;
		int int_flag = 0;
		double ds1, ds2;//gavriel 5.5.16
		float tmp_acc;

		////if(ist >= *jp-2) 
		if (*ist >= jp - 2)
		{
			//ist = *jp-1;
			*ist = jp - 1;
			sd[*ist] = sdfinal; // final point 
		}
		//else		
		//	if(posture[ist-1] ==posture[ist]) 
		//		sd[ist]=sdl[ist]-0.1; 	// start from tangency point (on limit curve)

		// printf("%f\n",ist);
		i = *ist;

		// start loop

		while (int_flag == 0)
		{
			sd1 = sd[i];
			ds = s[i] - s[i - 1];

			//	accl(i,&sd[i],&acc,&dec);//cancled gavriel 3.5.16
			accl_(i, sd[i], data, a_slide[i], b_slide[i], c_slide[i], gkt[i], &acc, &dec);
			acca = dec;
			if ((acc - dec) < -0.001)      // < 0: cross limit curve
			{
				////*icr = i;
				*icr = i;
				//	printf(" missed a hit point %d %f\n", i, ist); 
			}

			intb(sd1, ds, dec, &sd2, &dt, &ier);
			acc_vec[i - 1] = dec;
			//sdd_vec[i - 1] = dec;//gavriel 9.8.16


			//if(posture[i-1] == 0) 
			//if(sd2 > sdmax_front) sd2 = sdmax_front;
			/*		if(sd2 > sdmax)
			sd2 = sdmax;*/ //temp cancled gavriel 2.5.16///////////////////////////
			//if(posture[i-1] == 1) 
			//	if(sd2 > sdmax_side) sd2 = sdmax_side;
			//if(posture[i-1] == 2) 
			//	if(sd2 > sdmax_crwl) sd2 = sdmax_crwl;

			if (sd2 > sdl[i - 1]) 	// hit limit curve
			{
				//gavriel 2.5.16 - if integrated velocity cross limit curve - try move at limit curve speed - if the limit curve is real, it is not possible. 

				sd2 = sdl[i - 1];// - 0.1;
				tmp_acc = -(pow(sd2, 2) - pow(sd1, 2)) / (2 * ds);//find new acc 
				if (tmp_acc < acc && tmp_acc> dec)
				{
					// sd2 = sdmax;
					//ds1 = (pow(sd2,2) - pow(sd1,2))/(-2*dec);
					//ds2 = ds - ds1;
					//dt = ((double) ds2)/((double) sd2) + (- sd2  + (double) sd1)/((double) dec);

					if (abs(tmp_acc) > 0.0001)
						dt = (-sd2 + (double)sd1) / ((double)tmp_acc);
					else
						dt = ((double)ds) / ((double)sd2);


				}
				//sdd_vec[i - 1] = tmp_acc;//gavriel 9.8.16
				acc_vec[i - 1] = tmp_acc;

				//sd2 = sdl[i-1] - 0.1;
				//	printf("hit in backward integration\n"); 
				//	*icr = i;
			}

			if (sd2 > sd[i - 1]) 	// crossed
			{
				*icr = i;
				int_flag = 1;
				return;//temp cancled gavriel 2.5.16
			}

			sd[i - 1] = sd2;/////////////////////////////////////////////////////////////////////////
			t[i - 1] = dt;


			if (i < 2)
			{
				////*icr =*jp;
				*icr = jp;
				return;
			}
			i = i - 1;
		}

		return;

	}


	//c this version of slope does not interpolate and update  for tangential point
	//c slope search switching point backward
	//c dynamic version of subroutine slope without precompute limit velocity
	//c subroutine slope is to adapt the slope checking method to find 
	//c  the  critical and tangential point

	////void slope(int ih,int *jp, int *it)
	void slope(int *ih, int jp, int *it, float s[], float sd[], float sdl[], VehicleData data, float a_slide[], float b_slide[], float c_slide[], float gkt[])
	{

		float  sddl, sddm;
		int i;
		float acclow;

		//  search for slope difference change to identify critical or tangency point.
		i = *ih;
		////while(i<*jp-1)
		while (i< jp - 1)
		{
			//accl(i,&sdl[i], &sddm,&acclow); 
			accl_(i, sdl[i], data, a_slide[i], b_slide[i], c_slide[i], gkt[i], &sddm, &acclow);
			sddl = sdl[i] * (sdl[i + 1] - sdl[i]) / (s[i + 1] - s[i]);	// slope acceleration
			if (sddl > sddm)  	 // bingo
			{
				//	printf(", 'bingo: tangency point: %f %f\n",sddl, sddm);
				////*it = i;
				*it = i;
				sd[i] = sdl[i];//-.1;  // 4-8-06 shiller-cancel -.1 gavriel 17.5.16

							   //i = i + 1;  // 3-8-06 shiller
							   //sd[i] = sdl[i];  // 3-8-06 shiller
				return;
			}
			i = i + 1;
			sd[i] = sdl[i];
			////*it = i;
			*it = i;
		}
		return;

	}


	// 1-4-06 shiller

	//extern float f_max, f_min, mass;
	//extern float sdl[1000];  // 31-3-06 shiller
	//extern float xss[1000];

	/*actuator constraints*/
	//void accl(int index, float *sdp, float *acchigh, float *acclow)
	//{
	//	//int index;
	//	float sd, sdli;
	//	float sd2, square, accmax, accmin;
	//
	//	*acchigh = -1000.;
	//	*acclow = 1000.;
	//	// index= *indexp;
	//	sd = *sdp;
	//	sdli = sdl[index];
	//
	//	sd2 = sd * sd;
	//	square = (float)(-xss[index] * xss[index] * sd2*sd2 + fc*fc*g*g);
	//	if (ABS((square))<1e-5)
	//		square = 0.;
	//	else if (square <0)
	//		return;
	//	square = (float)(sqrt(square));
	//	accmax = f_max / mass;
	//	accmin = f_min / mass;
	//	*acchigh = ((accmax > square) ? square : accmax);
	//	*acclow = ((accmin >(-square)) ? accmin : (-square));
	//
	//
	//	return;
	//}

	bool traj_shrt(float sdinit, float sdfinal, int jp, float s[], float sd[], float sdl[], float t[],float acc_vec[], VehicleData data, float a_slide[], float b_slide[], float c_slide[], float gkt[])
	{

		int  ist;
		int ih, icr, ihn, itn;
		int it;                 // tangential point
		int i, j;
		extern int ier;
		//int ih_posture;

		int* ihv = new int[jp];
		int* itv = new int[jp];
		
		int int_flag = 0;

		float simulator_vel_scale = 1.0;

		for (i = 0; i<jp; i++)
		{
			sd[i] = -1;
		}

		//for (i=0;i<jp;i++)
		//	printf("sdl: %f \n",sdl[i]);

		int int_error = 0;   // 6-11-2015 shiller 
		ihn = 0;
		itn = 0;
		// set integration starting point
		ist = 0;
		ih = 0; // 3-8-06 shiller
		icr = 0; // 3-8-06 shiller
		it = 0;

		sd[0] = sdinit;

		//   forward integration

		while (int_flag == 0)
		{
			//skip:	
			//intgf(ist,in,t,&ih, &ih_posture);
			////intgf(ist,&jp,&ih);  // 31-3-06 shiller

			int_error = intgf(&ist, jp, &ih, s, sd, sdl, t, acc_vec, sdinit, data, a_slide, b_slide, c_slide, gkt);  // 31-3-06 shiller  6-11-2015 shiller


			if (int_error == 1)
			{
				//*total = 1000.;  // 6-11-2015 shiller 
				printf("error in traj_shrt \n");
				return false;
			}

			//if(ih_posture !=0) 
			//{
			//	itn = itn + 1;
			//	itv[itn-1] = ih+1;
			//	ist = ih+1;
			//	goto skip;
			//}
			ihn = ihn + 1;
			if (ihn == 60)
			{
				int_flag = 1;   // integrate backwards
				goto int_back;
				////return;
			}
			ihv[ihn - 1] = ih;
			////if(ih == in)  
			////if(ih == *jp)  
			if (ih == jp)
			{
				////ist = in;        
				////ist = *jp;        
				ist = jp;
				int_flag = 1;
				goto int_back;
				////return;
			}

			////slope(ih, &jp,  &it); 	// find next trangency or critical point
			slope(&ih, jp, &it, s, sd, sdl, data, a_slide, b_slide, c_slide, gkt); 	// find next trangency or critical point
			itn = itn + 1;
			if (it <= ih)

			{
				//it = ih + 1;
				sd[it] = sdl[it];//-.1;  // 4-8-06 shiller// cancel .1 gavriel 17.5.16
			}

			itv[itn - 1] = it;
			//	printf("%d %d\n",ih, it);
			ist = it;
			////if(it >= in-4) 
			////{
			////	for(j = it-1;j< in;j++)
			////if(it >= *jp-4) 
			if (it >= jp - 4)
			{
				////for(j = it-1;j< *jp;j++)
				for (j = it - 1; j< jp; j++)
					sd[j] = sdl[j];
				int_flag = 1;		// integrate backward
				goto int_back;  // 3-11-2015 shiller   goto was missing 
								////return;
			}
		}

		//return;

		// backward integration

	int_back:
		for (j = 0; j<itn; j++)
		{
			ist = itv[j];
			//intgb(ist,in,t,&icr);
			////intgb(ist,&jp,&icr); // 31-3-06 shiller
			intgb(&ist, jp, &icr, s, sd, sdl, t, acc_vec, sdinit, sdfinal, data, a_slide, b_slide, c_slide, gkt); // 31-3-06 shiller
		}
		////ist = in;
		////ist = *jp;
		ist = jp;
		//intgb(ist,in,t,&icr);
		////intgb(ist,&jp,&icr); // 31-3-06 shiller
		intgb(&ist, jp, &icr, s, sd, sdl, t, acc_vec, sdinit, sdfinal, data, a_slide, b_slide, c_slide, gkt); // 31-3-06 shiller

																									 // caculcate total time
																									 //*total = 0.;
																									 ////t[in-1]=0.;
																									 ////for(i=0;i<in;i++)
																									 ////t[*jp-1]=0.;


																									 //velocity scale for simulator experiments///////////////////////////////////////////////////////////////////////////////////
		for (i = 0; i < jp; i++)
		{
			sd[i] *= simulator_vel_scale;
		}
		t[0] = 0;
		for (i = 1; i < jp; i++)
		{
			t[i] = (s[i] - s[i - 1]) / (sd[i]);
		}

		t[jp - 1] = 0.;
		////for(i=0;i<*jp;i++)
		//for (i = 0; i<jp; i++)
		//	*total = *total + (float)(t[i]);
		////printf("Time = %f\n", *total);

		for (i = 0; i < jp; i++)
		{
			if (sd[i] == -1)
			{
				//printf("error in traj_shrt - not all points ok\n");
				//*total = 1000.;  // 6-11-2015 shiller 
				return false;
			}
		}
		acc_vec[jp - 1] = acc_vec[jp - 2];

		
		delete[] ihv;
		delete[] itv;

		return true;

	}





	int compute_limit_curve_and_velocity(int lenght, float* x, float* y, float* z, float init_velocity, float final_velocity,
		float f_max, float mass, float vel_max, float friction_coefficient, float height, float width,
		float* limit_curve, float* velocity, float* time,float* acc)
	{
		Path path(lenght, x, y, z);
		VehicleData data;
		data.f_max = f_max;
		data.mass = mass;
		data.sdmax = vel_max;
		data.friction_coefficient = friction_coefficient;
		data.height = height;
		data.width = width;

		float* gkt = new float[lenght];
		float* a_slide = new float[lenght];
		float* b_slide = new float[lenght];
		float* c_slide = new float[lenght];
		limitc(path, data, limit_curve, a_slide, b_slide, c_slide, gkt);

		traj_shrt(init_velocity, final_velocity, path.lenght, path.s, velocity, limit_curve, time,acc, data, a_slide, b_slide, c_slide, gkt);
		delete[] gkt;
		delete[] a_slide;
		delete[] b_slide;
		delete[] c_slide;
		return 1;
	}

//__declspec(dllexport) int compute_limit_curve(int lenght,float* x, float* y, float* z, float* limit_curve)//float pxyz[][3],#int lenght,  float limit_curve[]
//{
//	
//	Path path(lenght, x,y,z);
//	VehicleData data;
//	data.f_max = 4195;
//	data.mass = 3020;
//	data.sdmax = 30;
//	data.friction_coefficient = 0.7;
//	data.height = 0.86;
//	data.width = 2.08;
//	limitc(path, data, limit_curve);
//	return 1;
//}


// }
