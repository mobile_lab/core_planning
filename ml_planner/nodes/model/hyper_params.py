#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from collections import OrderedDict

class TrainHyperParameters:
    def __init__(self):
        self.alpha = 0.0001# #learning rate
        self.batch_size = 64
        self.replay_memory_size = 100000
        self.train_num = 100# how many times to train in every step
        self.run_random_num = 'inf'
        self.vehicle_ind_data = OrderedDict([('vel_y',0),('steer',1)])  #, ('angular_vel_z',4)  , ('roll',2) ('vel_x',3),  ('angular_vel_z',4)           

