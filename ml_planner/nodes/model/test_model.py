import rospy
import std_msgs
from autoware_msgs.srv import Predict,PredictResponse

def predict_srv(x,Predictor):
    try:      
        resp1 = Predictor([1.0,0.5,0.3,0.2])
        return resp1.y
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

#pub = rospy.Publisher('x_input', autoware_msg.Predict, queue_size=10)
#predict_srv = rospy.Service('predict', Predict, self.predict_cb)
rospy.init_node('node_name')
r = rospy.Rate(10) # 10hz
rospy.wait_for_service('predict')
Predictor = rospy.ServiceProxy('predict', Predict)
while not rospy.is_shutdown():
    x = [1.5,0.2,0.3,0.5]
    rospy.loginfo(x)
    # y = predict_srv(x,Predictor)
    try:      
        resp1 = Predictor([1.0,0.5,0.3,0.2])
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

    rospy.loginfo(resp1.y)
    #print(y)
    # pub.publish(my_array_for_publishing)
    r.sleep()