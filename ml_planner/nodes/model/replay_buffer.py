#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# import rospy
# import numpy as np
# import tensorflow as tf

# class RosTensorFlow():
#     def __init__(self):
#         self._session = tf.Session()

#         self._sub = rospy.Subscriber('image', Image, self.callback, queue_size=1)
#         self._pub = rospy.Publisher('result', String, queue_size=1)
#         self.score_threshold = rospy.get_param('~score_threshold', 0.1)
#         self.use_top_k = rospy.get_param('~use_top_k', 5)

#     def callback(self, image_msg):
#         cv_image = self._cv_bridge.imgmsg_to_cv2(image_msg, "bgr8")
#         # copy from
#         # classify_image.py
#         image_data = cv2.imencode('.jpg', cv_image)[1].tostring()
#         # Creates graph from saved GraphDef.
#         softmax_tensor = self._session.graph.get_tensor_by_name('softmax:0')
#         predictions = self._session.run(
#             softmax_tensor, {'DecodeJpeg/contents:0': image_data})
#         predictions = np.squeeze(predictions)
#         # Creates node ID --> English string lookup.
#         node_lookup = classify_image.NodeLookup()
#         top_k = predictions.argsort()[-self.use_top_k:][::-1]
#         for node_id in top_k:
#             human_string = node_lookup.id_to_string(node_id)
#             score = predictions[node_id]
#             if score > self.score_threshold:
#                 rospy.loginfo('%s (score = %.5f)' % (human_string, score))
#                 self._pub.publish(human_string)

        

# if __name__ == '__main__':

#     rospy.init_node('rostensorflow')
#     # tensor = RosTensorFlow()
#     # tensor.main()
#     print("helloooooo1")
#     print(tf.reduce_sum(tf.random.normal([1000, 1000])))
#     rospy.spin()

import tensorflow as tf
import rospy

class ModelWrapper():
    def __init__(self):
        # store the session object from the main thread
        self.session = tf.compat.v1.keras.backend.get_session()

        self.model = tf.keras.Sequential()
        # ...

class RosInterface():
    def __init__(self):
        self.wrapped_model = ModelWrapper()

def main():
    rospy.init_node("ros_tensorflow")
    ri = RosInterface()
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == "__main__":
    main()
