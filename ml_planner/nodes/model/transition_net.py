#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function



import tensorflow as tf
import sys
import pathlib

import rospy



def create_model(X_n,Y_n,alpha,batch_normalization = True,seperate_nets = True):
    # hidden_layer_nodes1 = 100
    hidden_layer_nodes1 = 100
    hidden_layer_nodes2 = 100
    hidden_layer_nodes3 = 100

    separate_layers_nodes = 20
    #tf.reset_default_graph()  
    #g = tf.Graph()
    #with g.as_default():

    input = tf.keras.Input(shape = [X_n])
    if seperate_nets:
        if batch_normalization:
            net = tf.keras.layers.BatchNormalization()(input)
            net = tf.keras.layers.Dense(hidden_layer_nodes1, activation=tf.keras.activations.relu)(input)
        else:
            net = tf.keras.layers.Dense(hidden_layer_nodes1, activation=tf.keras.activations.relu)(input)
        outputs = []
        for _ in range(Y_n):
            fc = tf.keras.layers.Dense(separate_layers_nodes, activation=tf.keras.activations.relu)(net)
            fc = tf.keras.layers.Dense(separate_layers_nodes, activation=tf.keras.activations.relu)(fc)
            outputs.append(tf.keras.layers.Dense(1)(fc))
        output = tf.keras.layers.concatenate(outputs)
        #model = tf.keras.Model(inputs=input,outputs=output_conc)
    else:
        if not batch_normalization:
            #input = tf.keras.Input(shape = [X_n])
            net = tf.keras.layers.Dense(hidden_layer_nodes1, activation=tf.keras.activations.relu)(input)
            net = tf.keras.layers.Dense(hidden_layer_nodes2, activation=tf.keras.activations.relu)(net)
            net = tf.keras.layers.Dense(hidden_layer_nodes3, activation=tf.keras.activations.relu)(net)
            output = tf.keras.layers.Dense(Y_n)(net)
            # model = tf.keras.Model(inputs=input,outputs=output)
        else:
            #input = tf.keras.Input(shape = [X_n])
            net = tf.keras.layers.BatchNormalization()(input)
            net = tf.keras.layers.Dense(hidden_layer_nodes1)(net)
            #net = tf.keras.layers.BatchNormalization()(net)
            net = tf.keras.layers.Activation('relu')(net)
            net = tf.keras.layers.Dense(hidden_layer_nodes2)(net)
            #net = tf.keras.layers.BatchNormalization()(net)
            net = tf.keras.layers.Activation('relu')(net)
            net = tf.keras.layers.Dense(hidden_layer_nodes3)(net)
            #net = tf.keras.layers.BatchNormalization()(net)
            net = tf.keras.layers.Activation('relu')(net)
            output = tf.keras.layers.Dense(Y_n)(net)


    model = tf.keras.Model(inputs=input,outputs=output)
    #model = tf.keras.models.Sequential([
    #        tf.keras.layers.Dense(hidden_layer_nodes1, activation=tf.keras.activations.relu, input_shape=(X_n,)),
    #        tf.keras.layers.Dense(hidden_layer_nodes2, activation=tf.keras.activations.relu),
    #        tf.keras.layers.Dense(hidden_layer_nodes3, activation=tf.keras.activations.relu),
    #        tf.keras.layers.Dense(Y_n)
    #        ])
    model.compile(optimizer=tf.keras.optimizers.Adam(),
            loss=tf.keras.losses.mean_squared_error
            )#metrics=['mae']
    print("Network ready")

   # graph =  tf.get_default_graph()#tf.compat.v1.get_default_graph()#
    return model#,graph

class Net:#define the input and outputs to networks, and the nets itself.
    def __init__(self,trainHP): 
        X_n = len(trainHP.vehicle_ind_data)+2# + acc-action, steer-action
        Y_n = len(trainHP.vehicle_ind_data) + 3 #+dx, dy, dang
        #self.TransNet = model_based_network(X_n,Y_n,trainHP.alpha)
        self.session = tf.compat.v1.keras.backend.get_session()
        self.TransNet = create_model(X_n,Y_n,trainHP.alpha,seperate_nets = True)
        
        self.TransNet._make_predict_function()
      
        self.restore_error = False
    def predict(self,x):
        rospy.loginfo("set seesion")
        with self.session.graph.as_default():
            tf.compat.v1.keras.backend.set_session(self.session)
            rospy.loginfo("done")
            return self.TransNet.predict(x)
    def restore_all(self,restore_file_path,name):
        try:
            path = restore_file_path +name+"/"
            self.TransNet.load_weights(path + "TransNet.ckpt")
            print("network restored")
            self.restore_error = False
        except:
            print('cannot restore net',sys.exc_info()[0])
            #raise
            self.restore_error = True

    def save_all(self,save_file_path,name):
        #self.TransNet.save_model(save_file_path)      
        path = save_file_path +name+"/"
        if not pathlib.Path(path).exists():  
            pathlib.Path(path).mkdir(parents=True) 
        self.TransNet.save_weights(path+"TransNet.ckpt ")



