#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# import rospy
# import numpy as np
# import tensorflow as tf

# class RosTensorFlow():
#     def __init__(self):
#         self._session = tf.Session()

#         self._sub = rospy.Subscriber('image', Image, self.callback, queue_size=1)
#         self._pub = rospy.Publisher('result', String, queue_size=1)
#         self.score_threshold = rospy.get_param('~score_threshold', 0.1)
#         self.use_top_k = rospy.get_param('~use_top_k', 5)

#     def callback(self, image_msg):
#         cv_image = self._cv_bridge.imgmsg_to_cv2(image_msg, "bgr8")
#         # copy from
#         # classify_image.py
#         image_data = cv2.imencode('.jpg', cv_image)[1].tostring()
#         # Creates graph from saved GraphDef.
#         softmax_tensor = self._session.graph.get_tensor_by_name('softmax:0')
#         predictions = self._session.run(
#             softmax_tensor, {'DecodeJpeg/contents:0': image_data})
#         predictions = np.squeeze(predictions)
#         # Creates node ID --> English string lookup.
#         node_lookup = classify_image.NodeLookup()
#         top_k = predictions.argsort()[-self.use_top_k:][::-1]
#         for node_id in top_k:
#             human_string = node_lookup.id_to_string(node_id)
#             score = predictions[node_id]
#             if score > self.score_threshold:
#                 rospy.loginfo('%s (score = %.5f)' % (human_string, score))
#                 self._pub.publish(human_string)

        

# if __name__ == '__main__':

#     rospy.init_node('rostensorflow')
#     # tensor = RosTensorFlow()
#     # tensor.main()
#     print("helloooooo1")
#     print(tf.reduce_sum(tf.random.normal([1000, 1000])))
#     rospy.spin()

import tensorflow as tf
import rospy
from collections import OrderedDict

class TrainHyperParameters:
    def __init__(self):
        self.alpha = 0.0001# #learning rate
        self.batch_size = 64
        self.replay_memory_size = 100000
        self.train_num = 100# how many times to train in every step
        self.run_random_num = 'inf'
        self.vehicle_ind_data = OrderedDict([('vel_y',0),('steer',1)])  #, ('angular_vel_z',4)  , ('roll',2) ('vel_x',3),  ('angular_vel_z',4)           



class Net:#define the input and outputs to networks, and the nets itself.
    def __init__(self,trainHP): 
        X_n = len(trainHP.vehicle_ind_data)+2# + acc-action, steer-action
        Y_n = len(trainHP.vehicle_ind_data) + 3 #+dx, dy, dang
        #self.TransNet = model_based_network(X_n,Y_n,trainHP.alpha)
        self.session = tf.compat.v1.keras.backend.get_session()
        self.TransNet,self.transgraph = keras_model.create_model(X_n,Y_n,trainHP.alpha,seperate_nets = True, normalize = trainHP.normalize_flag,mean= trainHP.features_mean,var = trainHP.features_var)
        
        self.TransNet._make_predict_function()
      
        self.restore_error = False
    def restore_all(self,restore_file_path,name):
        try:
            path = restore_file_path +name+"/"
            self.TransNet.load_weights(path + "TransNet.ckpt")
            print("network restored")
            self.restore_error = False
        except:
            print('cannot restore net',sys.exc_info()[0])
            #raise
            self.restore_error = True

    def save_all(self,save_file_path,name):
        #self.TransNet.save_model(save_file_path)      
        path = save_file_path +name+"/"
        if not pathlib.Path(save_path).exists():
            pathlib.Path(path).mkdir(parents=True) 
        self.TransNet.save_weights(path+"TransNet.ckpt ")



