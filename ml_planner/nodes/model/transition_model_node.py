#!/usr/bin/env python3
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import transition_net
import hyper_params
import numpy as np

import rospy
import std_msgs
from autoware_msgs.srv import Predict,PredictResponse


class TransitionModelNode:
    def __init__(self):
        s = rospy.Service('predict', Predict, self.state_and_action_callback)
        # self.pub_state = rospy.Publisher('y_output', std_msgs.msg.Float32MultiArray, queue_size=10)
        # rospy.Subscriber("x_input", std_msgs.msg.String, self.state_and_action_callback)

        self.trainHP = hyper_params.TrainHyperParameters()
        print("init")
        self.tr_net = transition_net.Net(self.trainHP)#define_and_restore_model

        # define_and_restore_replay_buffer()
        # train()
    def state_and_action_callback(self,srv_data):
        
        #print("callback",srv_data.x)
        # x1 =[[1.5,0.5,0.1,0.7]]
        rospy.loginfo(srv_data.x)
        y = self.tr_net.predict(np.array([srv_data.x]))[0]
        rospy.loginfo(y)
        #print(y)
        # s_next = predict_next(s)
        # publish(s_next)
        # self.pub_state.publish(y)#std_msgs.msg.String("foo")
        # save_s()
        return PredictResponse(y)

      




# class ModelWrapper():
#     def __init__(self):
#         # store the session object from the main thread
#         self.session = tf.compat.v1.keras.backend.get_session()

#         self.model = tf.keras.Sequential()
#         # ...

# class RosInterface():
#     def __init__(self):
#         self.wrapped_model = ModelWrapper()

def main():
    rospy.init_node("ml_model")
    tmn = TransitionModelNode()
    # ri = RosInterface()
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == "__main__":
    main()
