#import gui
import tkinker_gui
import threading
import time
import model_based_run
import shared
import hyper_parameters
import data_manager1
import environment1
import agent
import test_net_performance
import test_actions
import ML_library

# import rospy#tmp
class MF:
    def __init__(self):
        HP = hyper_parameters.safetyHyperParameters()
        # guiShared = shared.guiShared(HP.env_mode)
        # dataManager = data_manager1.DataManager(HP.save_file_path,HP.restore_file_path,HP.restore_flag)
        #initilize agent:
        # envData = environment1.OptimalVelocityPlannerData('model_based')

        self.Agent = agent.Agent(HP)


        if HP.program_mode == "train_in_env":
            print("Model free ready")

        elif HP.program_mode == "test_net_performance":
            test_net_performance.test_net(self.Agent)


        else:
            print("program_mode unkwnon:",HP.program_mode)
        
        # self.last_acc,self.last_steer = 0,0
        # self.predicted_path = [[0,0]]


    def get_action(self,full_state,time_error):
        state = self.Agent.get_state(full_state)
        if not self.Agent.HP.evaluation_flag:
            done = False
            self.Agent.add_to_replay(state,acc,steer,done,time_error, time_error)#save current state and actions applied on this state
        print("values:",state.Vehicle.values,"rel_pos:",state.Vehicle.rel_pos,state.Vehicle.rel_ang)    
        self.Agent.set_session()
        next_acc,next_steer,planningData,steer_error = self.Agent.comp_action(state,acc,steer)#self.last_acc,self.last_steer
        # self.last_acc,self.last_steer = acc,steer
        # self.predicted_path = planningData.vec_predicded_path[-1]
        # print("next_acc:",next_acc,"next_steer:",next_steer)
        
        return next_acc,next_steer,planningData.vec_predicded_path[-1],steer_error#return actions that should be applied at the next time step
    

    def reset_ML(self,full_state):
        self.Agent.set_last_pos(full_state)
    
    def end_ML_agent(self):
        self.Agent.set_session()
        # self.Agent.stop_training()
        self.Agent.save()#save nets and replay buffer
