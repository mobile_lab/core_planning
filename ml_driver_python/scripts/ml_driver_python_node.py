#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import numpy as np
import message_filters
import rospy
import std_msgs
# import model_based_main
#import model_based_main_class
from geometry_msgs.msg import TwistStamped, PoseStamped, AccelStamped, Point
from autoware_msgs.msg import Waypoint, Lane, LaneArray
from std_msgs.msg import Int32
from visualization_msgs.msg import Marker
import tf
import time
#from classes import Path
import ML_library
import library as lib
import agent
import test_net_performance



program_mode =  "train_in_env"#"test_net_performance" train_in_env, test_actions  timing
algorithm = "MF"

def create_full_state(local_path_ros, pose, twist):

    angles = tf.transformations.euler_from_quaternion([pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w])
    local_path = convert_from_ros_path(local_path_ros,pose.position.x,pose.position.y,angles[2])
    if local_path is None:
        return
    state = {'pos_x':pose.position.x,
             'pos_y':pose.position.y,
             'ang':1.57 - angles[2],
             'vel_x':twist.linear.y,
             'vel_y':twist.linear.x,
             'vel_z':twist.linear.z,
             'angular_vel_x':twist.angular.x,
             'angular_vel_y':twist.angular.y,
             'angular_vel_z':twist.angular.z,
            #  'acc_x':pl.simulator.vehicle.acceleration[0],
            #  'acc_y':pl.simulator.vehicle.acceleration[1],
            #  'acc_z':pl.simulator.vehicle.acceleration[2],
            #  'angular_acc_x':pl.simulator.vehicle.angular_acceleration[0],
            #  'angular_acc_y':pl.simulator.vehicle.angular_acceleration[1],
            #  'angular_acc_z':pl.simulator.vehicle.angular_acceleration[2],
            #  'wheel_n_vel':[pl.simulator.vehicle.wheels[0].vel_n,pl.simulator.vehicle.wheels[2].vel_n],#[wheel.vel_n for wheel in pl.simulator.vehicle.wheels],
             'steer':0,
             'roll':angles[0],
             'path':local_path
             }
    return state

#from autoware_msgs import 
def convert_from_ros_path(local_path,pos_x,pos_y,ang):
    path = ML_library.Path()
    for wp in local_path.waypoints:
        path.position.append([wp.pose.pose.position.x,wp.pose.pose.position.y,wp.pose.pose.position.z])
        path.analytic_velocity.append(wp.twist.twist.linear.x)
        path.analytic_velocity_limit.append(wp.twist.twist.linear.x+1)#because it is used later

    if len(path.position) < 2:
        print("path is too short")
        return None
        #comp distance along path and angles  - required later  
    path.comp_distance() 
    path.comp_angle()
    #transform to local frame (relative to vehicle). TODO transform in the perception module
    for i in range(len(path.position)):
        path.position[i] = lib.to_local(path.position[i],[pos_x,pos_y,0],1.57 - ang)# angles[2]
        path.angle[i] = [0.,ang - path.angle[i][1],0.]
    return path

class MLdriverNode:
    def __init__(self,Agent):
        self.Agent = Agent
        #self.pub_path = rospy.Publisher('/lane_waypoints_array',LaneArray , queue_size=10)
        self.pub_cmd = rospy.Publisher('/auto_twist_cmd',TwistStamped , queue_size=10)
        # self.sub_path = rospy.Subscriber("/lane_waypoints_array", LaneArray, self.path_callback)
        self.sub_mode_cmd = rospy.Subscriber("/mode_cmd", Int32, self.mode_cmd_callback)
        
        self.pub_mode_cmd = rospy.Publisher("/mode_cmd", Int32 , queue_size=10)
        self.pub_predicted_path = rospy.Publisher("/predicted_traj", Marker , queue_size=10)
        self.pub_target_point = rospy.Publisher("/next_target_mark", Marker , queue_size=10)
        self.first_flag = True
        self.path_in = False

        pose_sub = message_filters.Subscriber('current_pose_slow', PoseStamped)
        twist_sub = message_filters.Subscriber('current_velocity_slow', TwistStamped)
        local_path_sub = message_filters.Subscriber('final_waypoints', Lane)
        # acc_sub = message_filters.Subscriber('current_acceleration', AccelStamped)#_slow
        ts = message_filters.TimeSynchronizer([pose_sub, twist_sub, local_path_sub], 10)#, acc_sub
        ts.registerCallback(self.perception_callback)  

        self.last_t = rospy.get_time()
        self.ts = TwistStamped()

        self.acc,self.steer = 0,0
        self.steer_error = False

        self.mode = 0 #commands from manual interface
        self.changed_to_manual = False
        
    
    def mode_cmd_callback(self,mode):
        print("mode:",mode.data)
        
        if self.mode == 1 and mode.data == 0:
            self.changed_to_manual = True

        if self.mode == 0 and mode.data == 1:
            self.first_flag = True
        self.mode = mode.data

    # def send_cmd_callback(self,event):
    #     # print("time:",rospy.get_time() - self.pose.header.stamp.to_sec())
    #     self.pub_cmd.publish(self.ts)
        
      
    def perception_callback(self,pose,twist,local_path_ros):#,accel        
        self.pose = pose
        self.twist = twist


        #MB:
        if self.mode == 1:# or self.mode == 0
            if algorithm == "MB":
                self.send_drive_commands(self.acc,self.steer,self.twist.twist.linear.x, wait_for_dt = False)
            
            t = self.pose.header.stamp.to_sec() #rospy.get_time()
            self.dt = t - self.last_t
            self.last_t = t
            # print("time:", self.dt)
            # dt_send_receive = rospy.get_time() - self.pose.header.stamp.to_sec()
            # rospy.Timer(rospy.Duration(0.1 - dt_send_receive ), self.send_cmd_callback, oneshot=True)
            ddt = 0.01
            time_error = True if self.dt > 0.2 + ddt or self.dt < 0.2 - ddt else False
            #if time_error:
            print("dt:",self.dt)


            #self.publish_predicted_path_marker(local_path.position)
            full_state = create_full_state(local_path_ros,self.pose.pose, self.twist.twist)
            if algorithm == "MB":

                if self.first_flag:
                    self.Agent.set_last_pos(full_state)
                    self.first_flag = False
                    return


                t1 = rospy.get_time()
                self.acc,self.steer,predicted_path,self.steer_error = self.Agent.get_next_action(full_state,time_error,self.acc,self.steer)#compute action and save data for training
                print("total comp_time:",rospy.get_time() - t1)
                if self.steer_error:
                    self.publish_mode_cmd(0)

                
                self.publish_predicted_path_marker(predicted_path)



            elif algorithm == "MF":
                self.acc,self.steer = self.Agent.MFAgent.get_action(full_state,time_error)
                print("acc:",self.acc,"steer:",self.steer)
                self.send_drive_commands(self.acc,self.steer,self.twist.twist.linear.x, wait_for_dt = False)
            else:
                print("algorithm error") 


        if self.changed_to_manual:
            self.changed_to_manual = False
            self.Agent.save()   
            

        return

    def publish_mode_cmd(self,mode):
        mode_topic = Int32()
        mode_topic.data = mode
        self.pub_mode_cmd.publish(mode_topic)

    def publish_predicted_path_marker(self,path):
        self.path_marker = Marker()
        self.path_marker.header.frame_id = "/base_link"
        # self.path_marker.header.stamp = rospy.get_time()
        self.path_marker.type = Marker.LINE_STRIP
        self.path_marker.action = Marker.ADD
        self.path_marker.scale.x = 0.15
        self.path_marker.scale.y = 0.3
        self.path_marker.scale.z = 0.3
        self.path_marker.color.a = 0.9
        self.path_marker.color.r = 0.0
        self.path_marker.color.g = 0.0
        self.path_marker.color.b = 1.0
        for point in path:
            p = Point()
            p.x = point[1]
            p.y = -point[0]
            p.z = 0
            self.path_marker.points.append(p)
        self.pub_predicted_path.publish(self.path_marker)



    def publish_target_marker(self,target):
        self.target_marker = Marker()
        self.target_marker.header.frame_id = "/base_link"
        # self.target_marker.header.stamp = rospy.get_time()
        self.target_marker.type = Marker.SPHERE
        self.target_marker.action = Marker.ADD
        self.target_marker.scale.x = 1.0
        self.target_marker.scale.y = 1.0
        self.target_marker.scale.z = 1.0

        self.target_marker.color.a = 1.0
        self.target_marker.color.r = 0.0
        self.target_marker.color.g = 0.0
        self.target_marker.color.b = 1.0
        self.target_marker.frame_locked = True
         
        p = Point()
        p.x = target[1]
        p.y = -target[0]
        p.z = 0
        self.target_marker.pose.position = p
        self.pub_target_point.publish(self.target_marker)


    def send_drive_commands(self,acc,steer,vel, wait_for_dt = False):
        dvel = 1.0#0.5
        des_vel = np.clip(vel + acc*dvel,0,30)
        #print("acc:",acc,"steer:",steer)
        self.ts.twist.linear.x = des_vel
        self.ts.twist.angular.z = steer/0.33#-1:1 from -19:19 deg, -0.33:0.33 rad
        if not wait_for_dt:
            self.pub_cmd.publish(self.ts)
        # t = self.pose.header.stamp.to_sec() #rospy.get_time()
        # print("time:", t - rospy.get_time())




    # def reset_position(self):
    #     pass



    # def wait_for_connection(self):
    #     while self.first_flag:
    #         rospy.sleep(0.5)
    #         print ("waiting for vehicle data")

        # def send_path(self,path):
    #     L = Lane()
    #     for i in range (len(path.position)):
    #         wp = Waypoint()
    #         wp.pose.pose.position.x = path.position[i][0]
    #         wp.pose.pose.position.y = path.position[i][1]
    #         wp.pose.pose.position.z = path.position[i][2]
    #         #wp.twist.twist.linear.x = path.velocity[i][0]
    #         L.waypoints.append(wp)
    #     LA = LaneArray()
    #     LA.lanes.append(L)
    #     # rospy.loginfo(LA)
    #     self.pub_path.publish(LA)
        # def get_pose_twist(self):
    #     return self.pose,self.twist

    # def get_vehicle_data(self, wait_for_new_data = True):
    #     if wait_for_new_data:
    #         while not self.new_data:
    #             time.sleep(0.00001)
    #         self.new_data = False
    #     angles = tf.transformations.euler_from_quaternion([self.pose.pose.orientation.x, self.pose.pose.orientation.y, self.pose.pose.orientation.z, self.pose.pose.orientation.w])
    #     # print("get_vehicle_data")
    #     time_error = True if self.dt > 0.205 else False
    #     accel = self.twist.twist.linear#tmp should be: self.accel.accel.linear
    #     return self.pose.pose.position,angles,self.twist.twist.linear,self.twist.twist.angular,accel , self.pose.header.stamp.to_sec(),time_error

    # def get_local_path(self):
    #     path = ML_library.Path()
    #     for wp in self.local_path.waypoints:
    #         path.position.append([wp.pose.pose.position.x,wp.pose.pose.position.y,wp.pose.pose.position.z])
    #         path.analytic_velocity.append(wp.twist.twist.linear.x)
    #         path.analytic_velocity_limit.append(wp.twist.twist.linear.x+1)#because it is used later
    #     return path
    
    # def get_path(self):
    #     while not self.path_in:
    #         rospy.sleep(0.5)
    #         print ("waiting for path")
    #     L = self.LA.lanes[0]
    #     path = ML_library.Path()
    #     for wp in L.waypoints:
    #         path.position.append([wp.pose.pose.position.x,wp.pose.pose.position.y,wp.pose.pose.position.z])
    #         path.analytic_velocity.append(wp.twist.twist.linear.x)
    #         path.analytic_velocity_limit.append(wp.twist.twist.linear.x+1)#because it is used later
    #     return path
    # def path_callback(self,LA):
    #     print("path callback")
    #     self.LA = LA
    #     self.path_in =True







# class ModelWrapper():
#     def __init__(self):
#         # store the session object from the main thread
#         self.session = tf.compat.v1.keras.backend.get_session()

#         self.model = tf.keras.Sequential()
#         # ...

# class RosInterface():
#     def __init__(self):
#         self.wrapped_model = ModelWrapper()

def main():
    rospy.init_node("ml_driver")
    Agent = agent.Agent()
    if program_mode == "test_net_performance":
        test_net_performance.test_net(Agent)
    else:
        tmn = MLdriverNode(Agent)
    # model_based_main.main_MB(tmn)
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == "__main__":
    main()
