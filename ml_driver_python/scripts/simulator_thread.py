from classes import *
from communicationLib import Comm
import shared
import threading
import time
import copy

class SimVehicle:#simulator class - include communication to simulator, vehicle state and world state (recived states).
                 #
                 #also additional objects like drawed path

    def __init__(self,RosConnector):
        self.RosConnector = RosConnector
        self.RosConnector.wait_for_connection()
        self.vehicle = Vehicle()

    def send_drive_commands(self,velocity,steering):#send drive commands to simulator
        self.RosConnector.send_drive_commands(velocity,steering)
        error = 0
        return error
    def send_path(self,path_des):
        self.RosConnector.send_path(path_des)
        error = 0
        return error
    def send_predicted_path(self,path_pred):
        self.RosConnector.send_predicted_path(path_pred)
        error = 0
        return error

    def send_target(self,target):
        self.RosConnector.send_target(target)
        error = 0
        return error
    def get_path(self):
        return self.RosConnector.get_path()
    # def send_target_points(self,points):
    #     data_type = 5
    #     self.comm.serialize(data_type,int_flag = True)
    #     self.comm.serialize(len(points),int_flag = True)
    #     for point in points:
    #         self.comm.serialize(point[0])
    #     for point in points:
    #         self.comm.serialize(point[1])
    #         #comm.serialize(path_des.position[i].z)
    #     self.comm.sendData()
    #     self.comm.readData()
    #     data_type = self.comm.deserialize(1,int)
    #     error = 0
    #     if data_type == -1:
    #        print("error read data______________________________________________________________________")
    #        error = 1 
    #     return error

    def get_vehicle_data(self):
        position, angles,linear_vel,angular_vel,linear_accel, time_stamp,time_error = self.RosConnector.get_vehicle_data()

        # roll = euler[0]
        # pitch = euler[1]
        # yaw = euler[2]

        self.vehicle.position[0] = position.x
        self.vehicle.position[1] = position.y
        self.vehicle.position[2] = position.z  

        self.vehicle.angle[0] = angles[1]
        self.vehicle.angle[1] = 1.57 - angles[2]#-1.57
        self.vehicle.angle[2] = angles[0]
        #print("linear_vel:",linear_vel)
        self.vehicle.velocity[0] = linear_vel.y
        self.vehicle.velocity[1] = linear_vel.x
        self.vehicle.velocity[2] = linear_vel.z

        self.vehicle.angular_velocity[0] = angular_vel.x#0
        self.vehicle.angular_velocity[1] = angular_vel.y#0
        self.vehicle.angular_velocity[2] = angular_vel.z

        self.vehicle.acceleration[0] = linear_accel.y
        self.vehicle.acceleration[1] = linear_accel.x
        self.vehicle.acceleration[2] = linear_accel.z

        self.vehicle.last_time_stamp = time_stamp
        

        

        # print("pose:",self.vehicle.position)
        # print("angle:",self.vehicle.angle)

    #     self.vehicle.position = lib.changeZtoY(self.comm.deserialize(3,float))
    #     self.vehicle.position[2] = 0.
    #     self.vehicle.angle = lib.change_to_rad(self.comm.deserialize(3,float))
    #     if self.vehicle.angle[1] > math.pi:#angle form 0 to 2 pi, convert from -pi to pi
    #         self.vehicle.angle[1] = -(2*math.pi - self.vehicle.angle[1])
    #     if self.vehicle.angle[0] > math.pi:#angle form 0 to 2 pi, convert from -pi to pi
    #         self.vehicle.angle[0] = -(2*math.pi - self.vehicle.angle[0])
    #     if self.vehicle.angle[2] > math.pi:#angle form 0 to 2 pi, convert from -pi to pi
    #         self.vehicle.angle[2] = -(2*math.pi - self.vehicle.angle[2])
    #    # self.vehicle.backPosition = lib.changeZtoY(self.comm.deserialize(3,float))
    #     self.vehicle.velocity = lib.changeZtoY(self.comm.deserialize(3,float))
    #     self.vehicle.angular_velocity = lib.changeZtoY(self.comm.deserialize(3,float))#in radians
    #     self.vehicle.acceleration = lib.changeZtoY(self.comm.deserialize(3,float))
    #     self.vehicle.angular_acceleration = lib.changeZtoY(self.comm.deserialize(3,float))#in radians
    #     wheels_angular_vel = lib.change_to_rad(self.comm.deserialize(4,float))
    #     wheels_vel = self.comm.deserialize(8,float)
    #     j = 0
    #     for i in range(4):
    #         self.vehicle.wheels[i].angular_vel = wheels_angular_vel[i] 
    #         self.vehicle.wheels[i].vel_n = wheels_vel[j]
    #         self.vehicle.wheels[i].vel_t = wheels_vel[j+1]
    #         j+=2
    #     self.vehicle.steering = self.comm.deserialize(1,float)
    #     self.vehicle.last_time_stamp = self.comm.deserialize(1,float)
    #     self.vehicle.input_time = self.comm.deserialize(1,float)
        return False#time_error
    def reset_position(self):
        self.RosConnector.reset_position()
        return 0


def communication_loop(simulatorShared):
    print("loop")
    simulator = SimVehicle()   
    simulatorShared.vehicle
    try:
        simulator.connect()
        simulatorShared.connected = True
        print("connected")
    except:
        simulatorShared.connected = False
        print("cannot connect to simulator")

    while not simulatorShared.exit:
        simulator.get_vehicle_data()#update vehicle data continuesly
        with simulatorShared.Lock:
            simulatorShared.vehicle = copy.deepcopy(simulator.vehicle)#save last data in the shared resorce
        if simulatorShared.send_commands_flag:
            with simulatorShared.Lock:
                steering = simulatorShared.commands[1]
                acc = simulatorShared.commands[0]
            simulator.send_drive_commands(steering,acc)
            simulatorShared.send_commands_flag = False

        if simulatorShared.send_path_flag:
            with simulatorShared.Lock:
                path = copy.deepcopy(simulatorShared.path)
            simulator.send_path(path)
            simulatorShared.send_path_flag = False
        if simulatorShared.reset_position_flag:
            simulator.reset_position()
            simulatorShared.reset_position_flag = False
        if simulatorShared.end_connection_flag:
            simulator.comm.end_connection()
            simulatorShared.connected = False
            simulatorShared.exit = True
        time.sleep(0.01)
            
    
        

        
    

    
class LocalSimulator:#a local instance of the data in the simVehicle class. 
    def __init__(self,simulatorShared):
        self.simulatorShared = simulatorShared
        self.connected = False
        self.vehicle = Vehicle()
    def get_vehicle_data(self):
        with self.simulatorShared.Lock:
            self.vehicle = copy.deepcopy(self.simulatorShared.vehicle)

    def send_drive_commands(self,velocity,steering):#send drive commands to simulator
        with self.simulatorShared.Lock:
            self.simulatorShared.commands[0] = steering
            self.simulatorShared.commands[1] = velocity
            self.simulatorShared.send_commands_flag = True

    def send_path(self,path):
        with self.simulatorShared.Lock:
            self.simulatorShared.path = copy.deepcopy(path)
            self.simulatorShared.send_path_flag = True
    def reset_position(self):
        self.simulatorShared.reset_position_flag = True
    def end_connection(self):
        self.simulatorShared.end_connection_flag = True

class simulatorThread (threading.Thread):
   def __init__(self,simulatorShared):
        threading.Thread.__init__(self)
        self.simulatorShared = simulatorShared

              
   def run(self):
        print ("Starting " + self.name)
        communication_loop(self.simulatorShared)
        
        print ("Exiting " + self.name)
def start_simulator_connection():#start thread for connection with the simulator 

    simulatorShared = shared.simulatorShared()
    # Create new thread
    simThread = simulatorThread(simulatorShared)
    simThread.start()
    while simulatorShared.connected is None:
        time.sleep(0.1)
    localSim = LocalSimulator(simulatorShared)
    localSim.connected = simulatorShared.connected

    return localSim