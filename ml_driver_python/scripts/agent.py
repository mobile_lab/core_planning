import MB_action_lib
import agent_lib
import train_thread
import shared
import copy
import time
from model_based_net import model_based_network
import keras_model
import pathlib
import classes
import predict_lib
import library as lib
import numpy as np
import target_point
import actions_for_given_path as act
from DDPG_net import DDPG_network
import sys
import direct_method
import json
import agents_hyper_parameters
import random

class VehicleState:
    values = []
    abs_pos = []#x,y
    abs_ang = 0#angle
    rel_pos = []#dx,dy,
    rel_ang = 0 # dangle


class State:
    def __init__(self):
        self.Vehicle = VehicleState()#velocity, roll, current steering, ... 
        self.env = []# path, obstacles...
        return




class Nets:#define the input and outputs to networks, and the nets itself.
    def __init__(self,trainHP,trans_net_active = True,steer_net_active = True,acc_net_active = False): 
        self.trans_net_active,self.steer_net_active,self.acc_net_active = trans_net_active,steer_net_active,acc_net_active
        if self.trans_net_active:
            X_n = len(trainHP.vehicle_ind_data)+2# + acc-action, steer-action
            Y_n = len(trainHP.vehicle_ind_data) + 3 #+dx, dy, dang
            #self.TransNet = model_based_network(X_n,Y_n,trainHP.alpha)
            
            self.TransNet,self.transgraph,self.session  = keras_model.create_model(X_n,Y_n,trainHP.alpha,seperate_nets = True, normalize = trainHP.normalize_flag,mean= trainHP.features_mean,var = trainHP.features_var)
        
            self.TransNet._make_predict_function()
        if self.acc_net_active:
            X_n = len(trainHP.vehicle_ind_data) + 2 # + steer-action, desired roll
            Y_n = 1 #acc
            self.AccNet,_ = keras_model.create_model(X_n,Y_n,trainHP.alpha)#model_based_network(X_n,Y_n,trainHP.alpha)
        if self.steer_net_active:
            X_n = len(trainHP.vehicle_ind_data) + 2 # + acc-action, desired roll
            Y_n = 1#steer
            self.SteerNet,_ = keras_model.create_model(X_n,Y_n,trainHP.alpha,seperate_nets = False)
        self.restore_error = False
    def restore_all(self,restore_file_path,name):
        with self.transgraph.as_default(): 
            try:
                path = restore_file_path +name+"/"
                if self.trans_net_active:
                    self.TransNet.load_weights(path + "TransNet")#.ckpt
                if self.acc_net_active:
                    self.AccNet.load_weights(path + "AccNet.ckpt")
                if self.steer_net_active:
                    self.SteerNet.load_weights(path + "SteerNet.ckpt")
                print("networks restored")
                self.restore_error = False
            except:
                print('cannot restore net',sys.exc_info()[0])
                #raise
                self.restore_error = True

    def save_all(self,save_file_path,name):
        #self.TransNet.save_model(save_file_path)
        with self.transgraph.as_default(): 
            path = save_file_path +name+"/"
            if not pathlib.Path(path).exists():  
                pathlib.Path(path).mkdir(parents=True) 
            if self.trans_net_active:
                self.TransNet.save_weights(path+"TransNet")#.ckpt
                print("Nets are saved")
            if self.acc_net_active:
                self.AccNet.save_weights(path+"AccNet.ckpt")
            if self.steer_net_active:
                self.SteerNet.save_weights(path + "SteerNet.ckpt")


class MF_Net:#define the input and outputs to networks, and the nets itself.
    def __init__(self,trainHP,HP): 
        self.net = DDPG_network(trainHP.state_n,trainHP.action_n,\
                trainHP.alpha_actor,trainHP.alpha_critic,tau = trainHP.tau,seed = HP.seed[0],feature_data_n = trainHP.vehicle_ind_data, conv_flag = trainHP.conv_flag) 
        if HP.restore_flag:
            self.net.restore(HP.restore_file_path)#cannot restore - return true

class MFAgent:
    def __init__(self,HP,Replay,trainShared):
        self.HP = HP#required??
        self.trainShared = trainShared
        self.Replay = Replay
        self.trainHP = agents_hyper_parameters.MF_TrainHyperParameters()
        self.net = DDPG_network(self.trainHP.state_n,self.trainHP.action_n,
                self.trainHP.alpha_actor,self.trainHP.alpha_critic,tau = self.trainHP.tau,seed = HP.seed[0],
                feature_data_n = self.trainHP.vehicle_ind_data, conv_flag = self.trainHP.conv_flag)#MF_Net(trainHP,HP)

    def get_action(self,full_state,time_error):
        # self.targetPoint = target_point.comp_targetPoint(self.nets,state,self.trainHP)#tmp - must be computed independly from steps
  

        MF_state = self.convert_to_MF_state(full_state)#,targetPoint

        # self.set_session()
        self.trainShared.algorithmIsIn.clear()#indicates that are ready to take the lock
        with self.trainShared.Lock:
            self.trainShared.algorithmIsIn.set()
            #with self.net.transgraph.as_default():  
            action  = self.net.get_actions([MF_state])[0].tolist()
            action[0] += random.normalvariate(self.trainHP.noise_mu, self.trainHP.noise_sigma)
            action[1] += random.normalvariate(self.trainHP.noise_mu, self.trainHP.noise_sigma)
        print("reward:",agent_lib.compute_reward(MF_state,action,self.trainHP))
        done = False
        with self.trainShared.ReplayLock:
            self.Replay.add(copy.deepcopy((MF_state[:len(self.trainHP.vehicle_ind_data)],[0,0,0],action,MF_state[len(self.trainHP.vehicle_ind_data):],done,time_error,False)))#    
        return action[0],action[1]

    def convert_to_MF_state(self,full_state):#targetPoint
        # targetPoint = state.env[2]
        # return state.Vehicle.values+[targetPoint.abs_pos]+[targetPoint.vel]# + state.Vehicle.rel_pos+[state.Vehicle.rel_ang]
        values = []
        for feature in self.trainHP.vehicle_ind_data.keys():
            values.append(full_state[feature])
        path = full_state['path'].position[:self.trainHP.path_points_num]
        flat_path = []
        for pos in path:
            flat_path.append(pos[0])
            flat_path.append(pos[1])


        return values+flat_path# + state.Vehicle.rel_pos+[state.Vehicle.rel_ang]

    def start_training(self):
        #train the networks
        if self.trainHP.train_flag:
            print("start_training")

            #self.trainThread = train_thread.trainThread(self.train_nets,self.Replay,self.trainHP,self.HP,self.trainShared)
            self.trainThread = train_thread.MFtrainThread(self.net,self.Replay,self.trainHP,self.HP,self.trainShared)
            self.trainThread.start()
            #if self.trainHP.MF_policy_flag:
            #    self.MFtrainThread = train_thread.trainThread(self.nets,self.Replay,self.trainHP,self.HP,self.trainShared,self.nets.transgraph)
            #    self.MFtrainThread.start()

    def stop_training(self):
        self.trainShared.train = False
        time.sleep(1.0)
        self.trainShared.request_exit = True
        #while not self.trainShared.exit:
        #    print("waiting for exit train thread")
        #    time.sleep(0.1)
        print("exit from train thread")




            


class PlanningState:
    def __init__(self,trainHP):
        self.trust_T = 0
        self.var = [trainHP.init_var]+[min(trainHP.init_var+trainHP.one_step_var*n,trainHP.const_var ) for n in range(1,trainHP.rollout_n+10)]
        self.stabilize_var = [trainHP.init_var]+[min(trainHP.init_var+trainHP.one_step_emergency_var*n,trainHP.const_emergency_var ) for n in range(1,trainHP.rollout_n+10)]
        self.last_emergency_action_active = 0
            

 

class Agent:# includes the networks, policies, replay buffer, learning hyper parameters
    def __init__(self,trans_net_active = True,steer_net_active = False ,acc_net_active = False,one_step_var = None,const_var = None):
        self.HP = agents_hyper_parameters.HyperParameters()
        


        self.trainHP = agents_hyper_parameters.TrainHyperParameters(self.HP)
        self.var_name = "var"
        if one_step_var is not None:
            self.trainHP.one_step_var = one_step_var   
            self.trainHP.const_var = 1000.0 
        if const_var is not None:
            self.trainHP.const_var = const_var
            self.trainHP.one_step_var = 1000.0
            
        self.planningState = PlanningState(self.trainHP)
        self.Replay = agent_lib.Replay(self.trainHP.replay_memory_size)
        if self.HP.restore_flag:
            self.Replay.restore(self.HP.restore_file_path)

        self.Direct = direct_method.directModel(self.trainHP)
        #define all nets:
        self.nets = Nets(self.trainHP,trans_net_active,steer_net_active,acc_net_active)
        #self.train_nets = Nets(self.trainHP,trans_net_active,steer_net_active,acc_net_active)
        self.trainShared = shared.trainShared()
        self.MFAgent = MFAgent(self.HP,self.Replay,self.trainShared)
        self.MFAgent.start_training()

        if self.HP.restore_flag:
            #self.train_nets.restore_all(self.HP.restore_file_path,self.HP.net_name)
            #self.copy_nets()
            self.nets.restore_all(self.HP.restore_file_path,self.HP.net_name)
        self.start_training()
        return
    def set_last_pos(self,full_state):
        self.last_x,self.last_y,self.last_ang = full_state['pos_x'],full_state['pos_y'],full_state['ang']
    def save_nets(self):
        with self.trainShared.Lock:
            self.nets.save_all(self.HP.save_file_path,self.HP.net_name)
    def save(self):
        self.set_session()
        with self.trainShared.Lock:
            self.nets.save_all(self.HP.save_file_path,self.HP.net_name)
        self.Replay.save(self.HP.save_file_path)

    def start_training(self):
        #train the networks
        if self.trainHP.train_flag:
            print("start_training")
            #self.trainThread = train_thread.trainThread(self.train_nets,self.Replay,self.trainHP,self.HP,self.trainShared)
            self.trainThread = train_thread.trainThread(self.nets,self.Replay,self.trainHP,self.HP,self.trainShared)
            self.trainThread.start()
            #if self.trainHP.MF_policy_flag:
            #    self.MFtrainThread = train_thread.trainThread(self.nets,self.Replay,self.trainHP,self.HP,self.trainShared,self.nets.transgraph)
            #    self.MFtrainThread.start()

    def stop_training(self):
        self.trainShared.train = False
        time.sleep(1.0)
        self.trainShared.request_exit = True
        #while not self.trainShared.exit:
        #    print("waiting for exit train thread")
        #    time.sleep(0.1)
        print("exit from train thread")

    def set_session(self):
        keras_model.set_session(self.nets.session)
    def convert_to_planningData(self,state_env,StateVehicle_vec,actions_vec,StateVehicle_emergency_vec = None,actions_emergency_vec = None,emergency_action = False):#,targetPoint_vec = []
        planningData = classes.planningData()
      
        planningData.vec_path.append(state_env[0])
        #print("state.env:",state.env.position)
        planningData.vec_predicded_path.append([StateVehicle.abs_pos for StateVehicle in StateVehicle_vec])
        #planningData.vec_planned_roll.append([StateVehicle.values[self.trainHP.vehicle_ind_data["roll"]] for StateVehicle in StateVehicle_vec])
        planningData.vec_planned_roll.append([[0] for StateVehicle in StateVehicle_vec])
        planningData.vec_planned_vel.append([StateVehicle.values[self.trainHP.vehicle_ind_data["vel_y"]] for StateVehicle in StateVehicle_vec])
        planningData.vec_planned_acc.append([action[0] for action in actions_vec])
        planningData.vec_planned_steer.append([action[1] for action in actions_vec])
        if StateVehicle_emergency_vec is not None:
            planningData.vec_emergency_predicded_path.append([StateVehicle.abs_pos for StateVehicle in StateVehicle_emergency_vec])
            #planningData.vec_emergency_planned_roll.append([StateVehicle.values[self.trainHP.vehicle_ind_data["roll"]] for StateVehicle in StateVehicle_emergency_vec])
            planningData.vec_emergency_planned_roll.append([[0] for StateVehicle in StateVehicle_emergency_vec])
            planningData.vec_emergency_planned_vel.append([StateVehicle.values[self.trainHP.vehicle_ind_data["vel_y"]] for StateVehicle in StateVehicle_emergency_vec])
            planningData.vec_emergency_planned_acc.append([action[0] for action in actions_emergency_vec])
            planningData.vec_emergency_planned_steer.append([action[1] for action in actions_emergency_vec])
            planningData.vec_emergency_action.append(emergency_action)
        return planningData


    def comp_action(self,state,acc,steer):#env
        self.set_session()
        self.trainShared.algorithmIsIn.clear()#indicates that are ready to take the lock
        with self.trainShared.Lock:
            self.trainShared.algorithmIsIn.set()
            with self.nets.transgraph.as_default():  
                acc,steer,StateVehicle_vec,actions_vec,StateVehicle_emergency_vec,actions_emergency_vec,emergency_action,steer_error = act.comp_MB_action(self.nets,state,acc,steer,self.trainHP,
                                                                                                                                              planningState = self.planningState,
                                                                                                                                              Direct = self.Direct if (self.trainHP.direct_stabilize or self.trainHP.direct_predict_active or self.trainHP.direct_constrain) else None
                                                                                                                                              )
        self.planningState.last_emergency_action_active = emergency_action
        planningData = self.convert_to_planningData(state.env,StateVehicle_vec,actions_vec,StateVehicle_emergency_vec,actions_emergency_vec,emergency_action)
        return acc,steer,planningData,steer_error #act.comp_MB_action(self.nets.TransNet,env,state,acc,steer)
    


    def add_to_replay(self,state,acc,steer,done,time_error,fail):
        #self.trainShared.algorithmIsIn.clear()#indicates that are ready to take the lock
        with self.trainShared.ReplayLock:
            #self.trainShared.algorithmIsIn.set()
            #replay memory: [vehicle-state, rel_pos,action,done]
            self.Replay.add(copy.deepcopy((state.Vehicle.values,state.Vehicle.rel_pos+[state.Vehicle.rel_ang],[acc,steer],state.env,done,time_error,fail)))#      
              


    def get_state(self,full_state):#take from env what is nedded - the only connection to env.
        S = State()
        path = full_state['path']
        path.position = np.array(path.position)
        S.env = [path,0]#[full_state['path'],0]#local path
        S.Vehicle.rel_pos = lib.to_local([full_state['pos_x'],full_state['pos_y']],[self.last_x,self.last_y],self.last_ang)#relative position
        S.Vehicle.rel_ang = full_state['ang'] - self.last_ang#relative angle
        self.last_x,self.last_y,self.last_ang = full_state['pos_x'],full_state['pos_y'],full_state['ang']
        # S.Vehicle.rel_pos = [full_state['rel_pos_x'],full_state['rel_pos_y']]
        #print("rel_pos:",S.Vehicle.rel_pos)
        #S.Vehicle.rel_ang = full_state['rel_ang']
        S.Vehicle.abs_pos = [0.0,0.0]
        S.Vehicle.abs_ang = 0.0
        S.Vehicle.values = []
        for feature in self.trainHP.vehicle_ind_data.keys():
            S.Vehicle.values.append(full_state[feature])

        return S
    def copy_nets(self):
        t = time.time()#time.clock()
        with self.trainShared.Lock:
            if self.nets.trans_net_active:
                self.nets.TransNet.set_weights(self.train_nets.TransNet.get_weights()) 
            if self.nets.steer_net_active:
                self.nets.SteerNet.set_weights(self.train_nets.SteerNet.get_weights()) 
            if self.nets.acc_net_active:
                self.nets.AccNet.set_weights(self.train_nets.AccNet.get_weights()) 
        print("copy time:", time.time() - t)#time.clock()



    def update_episode_var(self,factor = 1.0):
        with self.trainShared.ReplayLock:
            if self.trainHP.var_update_steps is not None:
                var_update_steps = min(self.trainHP.var_update_steps,len(self.Replay.memory))
            else:
                var_update_steps = len(self.Replay.memory)
            episode_replay_memory = self.Replay.memory[-var_update_steps:]
            print("len(episode_replay_memory):",len(episode_replay_memory))

        with self.trainShared.Lock:
            n = self.trainHP.rollout_n
            n_state_vec,n_state_vec_pred,n_pos_vec,n_pos_vec_pred,n_ang_vec,n_ang_vec_pred = predict_lib.get_all_n_step_states(self.Direct if self.trainHP.direct_predict_active else self.nets.TransNet,
                                                                                                                                self.trainHP,
                                                                                                                                episode_replay_memory, n)
        #compute variance/abs_error for all raw features
        # var_vec,mean_vec,pos_var_vec,pos_mean_vec,ang_var_vec,ang_mean_vec = predict_lib.comp_var(self, n_state_vec,n_state_vec_pred,n_pos_vec,n_pos_vec_pred,n_ang_vec,n_ang_vec_pred,type = "mean_error")
        # val_var = var_vec[1]
        # #roll_var = [0]+[var[self.trainHP.vehicle_ind_data["roll"]] for var in var_vec]+[var_vec[-1][self.trainHP.vehicle_ind_data["roll"]]]*(20-len(var_vec)-1)#0.1
        # roll_var = [0]+[var[self.trainHP.vehicle_ind_data["roll"]] for var in var_vec]+[0.1]*(20-len(var_vec)-1)#0.1
        # print("roll_var:",roll_var)
        # self.planningState.var = roll_var

        #compute the variance/abs_error of the centripetal acceleration. 
        factor = 4
        var_vec = predict_lib.comp_ac_var(self, n_state_vec,n_state_vec_pred,type = "safety_std",factor = factor)#"mean_error" safety_std
        var_vec = [0]+var_vec+[1.0]*(20-len(var_vec)-1)#0.1
        print("var_vec:",var_vec)
        self.planningState.var = var_vec

    def save_var(self):
        path = self.HP.save_file_path +self.var_name+"/"
        pathlib.Path(path).mkdir(parents=True) 
        print("var path: ", path+"var.txt")
        try: 
            with open(path+"var.txt", 'w') as f:
                #json.dump((self.run_num,self.train_num,self.rewards,self.length,self.relative_reward, self.episode_end_mode,self.path_seed,self.paths ),f)
                json.dump((self.planningState.var),f)

            print("var saved")            
        except:
            print("cannot save var", sys.exc_info()[0])
    def load_var(self):
        path = self.HP.restore_file_path +self.var_name+"/"
        print("var path: ", path+"var.txt")
        try:
            with open(path+"var.txt", 'r') as f:
                #self.run_num,self.train_num,self.rewards,self.length,self.relative_reward, self.episode_end_mode,self.path_seed,self.paths = json.load(f)#,self.paths
                self.planningState.var = json.load(f)#,self.paths

            print("var restored")
            return False
        except:
            print ("cannot restore var:", sys.exc_info()[0])
            return True
    def get_next_action(self,full_state,time_error,acc,steer):
        state = self.get_state(full_state)
        # if not self.HP.evaluation_flag:
        done = False
        self.add_to_replay(state,acc,steer,done,time_error, time_error)#save current state and actions applied on this state
        print("values:",state.Vehicle.values,"rel_pos:",state.Vehicle.rel_pos,state.Vehicle.rel_ang)    
        
        next_acc,next_steer,planningData,steer_error = self.comp_action(state,acc,steer)#self.last_acc,self.last_steer
        # self.last_acc,self.last_steer = acc,steer
        # self.predicted_path = planningData.vec_predicded_path[-1]
        # print("next_acc:",next_acc,"next_steer:",next_steer)
        
        return next_acc,next_steer,planningData.vec_predicded_path[-1],steer_error#return actions that should be applied at the next time ste


        
        

