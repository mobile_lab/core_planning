# import MB_action_lib
# import agent_lib
# import train_thread
# import shared
# import copy
# import time
# from model_based_net import model_based_network
# import keras_model
# from collections import OrderedDict
# import pathlib
# import classes
# import predict_lib
import library as lib
import numpy as np
# import target_point
# import actions_for_given_path as act
# #from DDPG_net import DDPG_network
# import sys
# import direct_method
# import json
import math




class Path:
    def __init__(self):
        self.position = []#
        self.backPosition = []
        self.angle = []
        self.curvature = []
        self.velocity = [] #real velocity for a real path and planned velocity for a planned path
        self.steering = []
        self.distance = []
        self.time = []
        self.max_velocity = []#maximum velocity at each time - maximum allowed velocity
        self.analytic_velocity_limit = []#velocity limit at each point (from analitic compute)
        self.analytic_velocity = []
        self.analytic_acceleration = []
        self.analytic_time = []
        self.seed = None

    def dist(self,x1,y1,x2,y2):
        tmp = (x2-x1)**2 + (y2- y1)**2
        if tmp > 0:
            return math.sqrt(tmp)
        else:
            return 0.

    def comp_curvature(self):
        for i in range ( len(self.position)-2):#start from 0 up to end - 2
            pnt1 = np.array([self.position[i][0],self.position[i][1],self.position[i][2]])
            pnt2 = np.array([self.position[i+1][0],self.position[i+1][1],self.position[i+1][2]])
            pnt3 = np.array([self.position[i+2][0],self.position[i+2][1],self.position[i+2][2]])
            self.curvature.append(lib.comp_curvature(pnt1,pnt2,pnt3))
        self.curvature.append(self.curvature[-1])
        self.curvature.append(self.curvature[-1])
        #self.curvature = [abs(curv) for curv in self.curvature]

    def comp_distance(self):
        self.distance = []
        
        self.distance.append(0.)
        for i in range (1,len(self.position)):#start from 2
            self.distance.append(self.distance[i-1] + self.dist(self.position[i][0],self.position[i][1],self.position[i-1][0],self.position[i-1][1]))
        return
    def comp_angle(self):
        self.angle =[]
        for i in range (0,len(self.position)-1):#start from 0
           self.angle.append([0,-(math.atan2(self.position[i+1][1] - self.position[i][1],self.position[i+1][0] - self.position[i][0]) - math.pi/2),0])
        self.angle.append(self.angle[-1])
    def set_velocity(self, vel):
        self.velocity = []
        for i in range (len(self.position)):
            self.velocity.append(vel)
        return



