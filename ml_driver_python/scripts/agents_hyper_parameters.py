import os
from collections import OrderedDict

class MF_TrainHyperParameters:
    def __init__(self):
        self.batch_size = 64
        self.alpha_actor = 0.0001
        self.alpha_critic = 0.001
        self.gamma = 0.99
        self.tau = 0.001 
        self.noise_sigma = 0.1
        self.noise_mu = 0 
        self.conv_flag = False
        self.vehicle_ind_data = OrderedDict([('vel_y',0),('angular_vel_z',1)])
        self.path_points_num = 25
        self.state_n = len(self.vehicle_ind_data)+self.path_points_num*2
        self.action_n = 2
        self.train_flag = True
        self.evaluation_flag = False
        self.always_no_noise_flag = True
        ##
        self.reduce_vel = 0.0

class TrainHyperParameters:
    def __init__(self,HP):
        self.train_flag = False
        self.MF_policy_flag = False
        self.direct_predict_active = False#True# False use bicycle model for state prediction
        self.direct_constrain = True #stabilization constrain computed by direct model (centrpetal force limit) or roll constrain
        self.update_var_flag = False 
        self.var_update_steps = 2000
        self.num_of_runs = 1
        self.alpha = 0.0001# #learning rate
        self.batch_size = 64
        self.replay_memory_size = 100000
        self.train_num = 100# how many times to train in every step
        self.run_random_num = 'inf'
        self.vehicle_ind_data = OrderedDict([('vel_y',0),('angular_vel_z',1),('steer',1)])  #,('acc_x',3) ('steer',1) , ('angular_vel_z',4)  , ('roll',2) ('vel_x',3),  ('angular_vel_z',4)
        self.normalize_flag = False
        if self.normalize_flag:
            self.features_mean = [7,0,0,0,0]#input feature + action
            self.features_var = [7,0.5,0.05,0.7,0.7]
        else:
            self.features_mean = None
            self.features_var = None
        self.direct_stabilize = HP.direct_stabilize
        self.plan_roll = 0.03
        #self.emergency_plan_roll = 0.07
        self.target_tolerance = 0.02
        self.min_dis = 0.5#or precentage
        self.max_plan_deviation = 10
        self.max_plan_roll = 0.1
        self.init_var = 0.0#uncertainty of the roll measurment
        if self.update_var_flag:
            self.one_step_var =1.0
            self.const_var =1.0
        else:
            #0.5 not move. 0.2 < 0.5 of VOD. 0.1 =0.85 of VOD. 0 1+-0.05 of VOD com height = 1.7
            self.one_step_var =0.04#0.04# 0.02 is good
            self.const_var = 1000.0#0.05#roll variance at the future states, constant because closed loop control?
            self.one_step_emergency_var = 0.05
            self.const_emergency_var = 0.2
        self.prior_safe_velocity = 0.02#if the velocity is lower than this value - it is priori Known that it is OK to accelerate
        self.stabilize_factor = 1.0 #0.5#
        #self.emergency_const_var = 0.05
        self.pause_for_training = False
      
        self.emergency_action_flag = HP.emergency_action_flag
        self.emergency_steering_type = HP.emergency_steering_type#1 - stright, 2 - 0.5 from original steering, 3-steer net
     
        

        self.max_cost = 100
        self.rollout_n = 13#10
        if self.MF_policy_flag:
            self.MF_alpha_actor = 0.0001
            self.MF_alpha_critic = 0.001
            self.tau = 0.001 
            self.conv_flag = False
            self.state_n = len(self.vehicle_ind_data)+3
            self.action_n = 2

# class safetyHyperParameters:
#     def __init__(self):
#         self.gui_flag = False
#         #self.epsilon_start = 1.0
#         #self.epsilon = 0.1
#         self.gamma = 0.99
#         self.tau = 0.001 #how to update target network compared to Q network
#         self.num_of_runs = 100000
#         self.alpha_actor = 0.0001# for Pi 1e-5 #learning rate
#         self.alpha_critic = 0.001#for Q 0.001
#         self.alpha_analytic_actor = 1e-5#1e3 - too high, loss stop at 0.1
#         self.alpha_analytic_critic = 1e-4
#         self.batch_size = 64
#         self.replay_memory_size = 1000000
#         self.train_num = 2# how many times to train in every step
#         self.sample_ratio = 1.0
#         self.epsilon = 0.1
#         self.minQ = -0.5
#         #########################
#         self.env_mode = "SDDPG_pure_pursuit"#SDDPG DDPG_target SDDPG_pure_pursuit
#         self.evaluation_flag = False
#         self.reduce_vel = 0.0
#         self.add_feature_to_action  = False
#         self.analytic_action = False
#         self.train_flag =True
#         self.noise_flag = True
#         self.always_no_noise_flag = False
#         self.evaluation_every = 10
#         self.test_same_path = False
#         self.run_same_path = False
#         self.conv_flag = True
#         self.gym_flag = False
#         self.render_flag = True
#         self.plot_flag = True
#         self.stabilize_flag = False
#         self.constant_velocity = None #5.0
#         self.DQN_flag = False
#         #self.pure_persuit_flag = False
#         self.restore_flag = False  
#         self.skip_run = False
#         self.reset_every = 3
#         self.save_every = 99999999
#         self.save_every_train_number = 25000000
#         self.seed = [1111]#,1112,1113,1114,1115]
#         self.save_name ="SDDPG_direct_reward1"#SDDPG_vel_and_steer_roll_reward2- roll reward, no safety, ~0.8 of VOD 20-30% fails 
#         self.folder_path = os.getcwd()+ "/files/models/MB_paper_stabilize/"
#         #SDDPG_vel_and_steer_roll_reward3 -with roll feature, doesn't converge
#         self.save_file_path = self.folder_path+self.save_name+"/"

#         self.restore_name = "SDDPG_direct_reward1"#SDDPG_pure_pursuit safety good but limit velocity because reward to low. SDDPG_pure_persuit1 - good
#         #SDDPG_pure_persuit3 - conv_flag, path layer sizes 50,20
#         #SDDPG_pure_persuit3 - conv_flag, path layer sizes 20,5
#         self.restore_file_path = self.folder_path+self.restore_name+"/"

#         if self.always_no_noise_flag:
#             self.noise_flag = False

class HyperParameters:#global settings of the program.
    def __init__(self):
        self.emergency_action_flag = False
        self.emergency_steering_type = 1#1 - stright, 2 - 0.5 from original steering, 3-steer net, 4-same steering, 5-roll proportional
        self.direct_stabilize = True#use bicycle model for pridiction of states for stabilization   
        
        
        self.num_of_runs = 100000
        self.save_every_train_number = 25000000
        self.evaluation_every = 999999999
        #####################
        
        self.max_steps = 1500
        # self.train_flag = True
        #self.noise_flag = True
        #self.always_no_noise_flag = False
        self.analytic_action = False
       # self.zero_noise_every = 1
        self.evaluation_every = 10
        self.test_same_path = False
        self.run_same_path = False
       # self.conv_flag = False
        self.gym_flag = False
        self.render_flag = True
        self.plot_flag = True
        self.restore_flag = False# False True
        self.skip_run = False
        self.reset_every = 1
        self.save_every = 10000000000
        self.save_every_time = 5000 #minutes
        self.seed = [1111]
        self.net_name = "tf_model"
        self.save_name = "lgsvl11"#"MB_R_long2" MB_R_DS1
        self.folder_path = os.path.dirname(__file__) + "/files/models/test_lgsvl/"#MB_learning_process2/ MB_learning_process0.05/ MB_learning_process2_0.05/ MB_learning_process_no_stabilize1_0.05/
        #self.save_file_path = os.getcwd()+ "/files/models/model_based/"+self.save_name+"/"f
        self.save_file_path = self.folder_path +self.save_name+"/"
         
        self.restore_name = "lgsvl11"#"MB_R_long1"#MB_R_long2 MB_long01_short_train
        #self.restore_file_path = os.getcwd()+ "/files/models/model_based/"+self.restore_name+"/"
        self.restore_file_path = self.folder_path +self.restore_name+"/"

       
