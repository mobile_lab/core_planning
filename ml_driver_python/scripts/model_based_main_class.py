#import gui
import tkinker_gui
import threading
import time
import model_based_run
import shared
import hyper_parameters
import data_manager1
import environment1
import test_net_performance
import test_actions
import ML_library

# import rospy#tmp
class MB:
    def __init__(self):
        HP = hyper_parameters.ModelBasedHyperParameters()
        # guiShared = shared.guiShared(HP.env_mode)
        # dataManager = data_manager1.DataManager(HP.save_file_path,HP.restore_file_path,HP.restore_flag)
        #initilize agent:
        # envData = environment1.OptimalVelocityPlannerData('model_based')

        self.Agent = agent.Agent(HP)


        if HP.program_mode == "train_in_env":
            print("Model based ready")
            #initialize environment:
            
            # env = environment1.OptimalVelocityPlanner(dataManager,RosConnector,env_mode = "model_based")

            # guiShared.max_roll = envData.max_plan_roll
            # guiShared.max_time = envData.step_time*envData.max_episode_steps+5#add time for braking
            # if env.opened:     
            #     model_based_algorithm.train(env,HP,Agent,dataManager,guiShared,RosConnector)

        elif HP.program_mode == "test_net_performance":
            test_net_performance.test_net(self.Agent)

        # elif HP.program_mode == "test_actions":
        #     test_actions.test(Agent)

        else:
            print("program_mode unkwnon:",HP.program_mode)
        
        # self.last_acc,self.last_steer = 0,0
        # self.predicted_path = [[0,0]]


    def get_next_action(self,full_state, time_stamp,time_error,acc,steer):
        state = self.Agent.get_state(full_state)
        if not self.Agent.HP.evaluation_flag:
            done = False
            self.Agent.add_to_replay(state,acc,steer,done,time_error, time_error)#save current state and actions applied on this state
        print("values:",state.Vehicle.values,"rel_pos:",state.Vehicle.rel_pos,state.Vehicle.rel_ang)    
        self.Agent.set_session()
        next_acc,next_steer,planningData,steer_error = self.Agent.comp_action(state,acc,steer)#self.last_acc,self.last_steer
        # self.last_acc,self.last_steer = acc,steer
        # self.predicted_path = planningData.vec_predicded_path[-1]
        # print("next_acc:",next_acc,"next_steer:",next_steer)
        
        return next_acc,next_steer,planningData.vec_predicded_path[-1],steer_error#return actions that should be applied at the next time step


    def reset_ML(self,full_state):
        self.Agent.set_last_pos(full_state)
    
    def end_ML_agent(self):
        self.Agent.set_session()
        # self.Agent.stop_training()
        self.Agent.save()#save nets and replay buffer
